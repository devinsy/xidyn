/*
 * Copyright (C) 2006-2017 Christian Pierre MOMON
 * 
 * This file is part of Xidyn.
 * 
 * Xidyn is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Xidyn is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Xidyn.  If not, see <http://www.gnu.org/licenses/>
 */
package fr.devinsy.xidyn.utils;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.fest.assertions.Assertions;
import org.junit.Before;
import org.junit.Test;

/**
 * The Class XidynUtilsTest.
 */
public class XidynUtilsTest
{
    /**
     * Before.
     */
    @Before
    public void before()
    {
        BasicConfigurator.configure();
        Logger.getRootLogger().setLevel(Level.ERROR);
    }

    /**
     * Test extract body content 01.
     */
    @Test
    public void testExtractBodyContent01()
    {
        String source = "aaaaa<boDY>hello</Body>zzzzz";

        String target = XidynUtils.extractBodyContent(source);
        Assertions.assertThat(target).isEqualTo("hello");
    }

    /**
     * Test extract body content 02.
     */
    @Test
    public void testExtractBodyContent02()
    {
        String source = "aaaaaaa<boDY>hello</Bod>zzzzz";

        String target = XidynUtils.extractBodyContent(source);
        Assertions.assertThat(target).isEqualTo("");
    }

    /**
     * Test extract body content 03.
     */
    @Test
    public void testExtractBodyContent03()
    {
        String source = "aaaaa<body></BodY>zzzzz";

        String target = XidynUtils.extractBodyContent(source);
        Assertions.assertThat(target).isEqualTo("");
    }

    /**
     * Test extract body content 04.
     */
    @Test
    public void testExtractBodyContent04()
    {
        String source = "aaaaa<boDY>    hello     </body>zzzzz";

        String target = XidynUtils.extractBodyContent(source);
        Assertions.assertThat(target).isEqualTo("hello");
    }

    /**
     * Test extract body content 05.
     */
    @Test
    public void testExtractBodyContent05()
    {
        StringBuffer buffer = new StringBuffer(1000);
        buffer.append("<?xml version=\"1.0\" encoding=\"UTF-8\" ?>").append("\n");
        buffer.append("<!DOCTYPE html>").append("\n");
        buffer.append("<html xmlns=\"http://www.w3.org/1999/xhtml\">").append("\n");
        buffer.append("<head>").append("\n");
        buffer.append("<title>Kinsources</title>").append("\n");
        buffer.append("<meta charset=\"UTF-8\" />").append("\n");
        buffer.append("<link rel=\"icon\" type=\"image/x-icon\" href=\"/favicon.ico\" />").append("\n");
        buffer.append("	<link rel=\"stylesheet\" type=\"text/css\" href=\"kiwa.css\" />").append("\n");
        buffer.append("<meta content=\"kinsources, puck, devinsy, gedcom\" name=\"keywords\" />").append("\n");
        buffer.append("</head>").append("\n");
        buffer.append("<body>");
        buffer.append("WELCOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOME");
        buffer.append("</body>").append("\n");
        buffer.append("</html>").append("\n");

        String source = buffer.toString();

        // System.out.println("[" + source + "]");
        String target = XidynUtils.extractBodyContent(source);
        Assertions.assertThat(target).isEqualTo("WELCOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOME");
    }

    /**
     * Test extract doctype 01.
     */
    @Test
    public void testExtractDoctype01()
    {
        String source = "aaaaa<boDY>hello</Body>zzzzz";

        String target = XidynUtils.extractDoctype(source);
        Assertions.assertThat(target).isNull();
    }

    /**
     * Test extract doctype 02.
     */
    @Test
    public void testExtractDoctype02()
    {
        String source = "<html xmlns='http://www.w3.org/1999/xhtml'>aaaaa<boDY>hello</Body>zzzzz";

        String target = XidynUtils.extractDoctype(source);
        Assertions.assertThat(target).isEqualTo("");
    }

    /**
     * Test extract doctype 03.
     */
    @Test
    public void testExtractDoctype03()
    {
        String source = "<!DOCTYPE html><html xmlns='http://www.w3.org/1999/xhtml'>aaaaa<boDY>hello</Body>zzzzz";

        String target = XidynUtils.extractDoctype(source);
        Assertions.assertThat(target).isEqualTo("<!DOCTYPE html>");
    }

    /**
     * Test extract doctype 04.
     */
    @Test
    public void testExtractDoctype04()
    {
        String source = "<?xml version='1.0' encoding='UTF-8' ?><!DOCTYPE html><html xmlns='http://www.w3.org/1999/xhtml'>aaaaa<boDY>hello</Body>zzzzz";

        String target = XidynUtils.extractDoctype(source);
        Assertions.assertThat(target).isEqualTo("<?xml version='1.0' encoding='UTF-8' ?><!DOCTYPE html>");
    }

    /**
     * Test extract doctype 05.
     */
    @Test
    public void testExtractDoctype05()
    {
        String source = "<?xml version='1.0' encoding='UTF-8' ?><!-- TOTOT --><!DOCTYPE html><html xmlns='http://www.w3.org/1999/xhtml'>aaaaa<boDY>hello</Body>zzzzz";

        String target = XidynUtils.extractDoctype(source);
        Assertions.assertThat(target).isEqualTo("<?xml version='1.0' encoding='UTF-8' ?><!-- TOTOT --><!DOCTYPE html>");
    }
}
