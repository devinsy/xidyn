/*
 * Copyright (C) 2016-2018 Christian Pierre MOMON
 * 
 * This file is part of Xidyn.
 * 
 * Xidyn is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Xidyn is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Xidyn.  If not, see <http://www.gnu.org/licenses/>
 */
package fr.devinsy.xidyn.pages;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.fest.assertions.Assertions;
import org.junit.Before;
import org.junit.Test;

/**
 * The Class PageFactoryTest.
 */
public class PageFactoryTest
{
    /**
     * Before.
     */
    @Before
    public void before()
    {
        BasicConfigurator.configure();
        Logger.getRootLogger().setLevel(Level.INFO);
    }

    /**
     * Test page 01.
     * 
     * @throws Exception
     *             the exception
     */
    @Test(expected = IllegalArgumentException.class)
    public void testPage01() throws Exception
    {
        String source = null;

        String target = PageFactory.instance().get(source).dynamize().toString();

        Assertions.assertThat(target).isEqualTo(source);
    }

    /**
     * Test page 02.
     * 
     * @throws Exception
     *             the exception
     */
    @Test
    public void testPage02() throws Exception
    {
        String source = "aaaaa<div>hello</div>zzzzz";

        String target = PageFactory.instance().get(source).dynamize().toString();

        Assertions.assertThat(target).isEqualTo(source);
    }
}
