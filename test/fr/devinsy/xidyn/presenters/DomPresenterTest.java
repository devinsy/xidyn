/*
 * Copyright (C) 2006-2017 Christian Pierre MOMON
 * 
 * This file is part of Xidyn.
 * 
 * Xidyn is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Xidyn is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Xidyn.  If not, see <http://www.gnu.org/licenses/>
 */
package fr.devinsy.xidyn.presenters;

import org.fest.assertions.Assertions;
import org.junit.Test;

import fr.devinsy.xidyn.data.TagDataManager;

/**
 * The Class DomPresenterTest.
 */
public class DomPresenterTest
{
    /**
     * Test static dynamize 01.
     * 
     * @throws Exception
     *             the exception
     */
    @Test
    public void testStaticDynamize01() throws Exception
    {
        String source = "aaaaa<div>hello</div>zzzzz";

        String target = PresenterUtils.dynamize(source, null).toString();

        Assertions.assertThat(target).isEqualTo(source);
    }

    /**
     * Test static dynamize 02.
     * 
     * @throws Exception
     *             the exception
     */
    @Test
    public void testStaticDynamize02() throws Exception
    {
        String source = "aaaaa<div>hello</div>zzzzz";

        String target = PresenterUtils.dynamize(source, new TagDataManager()).toString();

        Assertions.assertThat(target).isEqualTo(source);
    }
}
