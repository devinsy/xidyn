/*
 * Copyright (C) 2006-2017 Christian Pierre MOMON
 * 
 * This file is part of Xidyn.
 * 
 * Xidyn is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Xidyn is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Xidyn.  If not, see <http://www.gnu.org/licenses/>
 */
import org.apache.log4j.Level;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.devinsy.xidyn.data.SimpleTagData;
import fr.devinsy.xidyn.data.TagDataManager;
import fr.devinsy.xidyn.presenters.PresenterUtils;

/**
 * The Class XidynTest.
 */
class XidynTest
{
    public enum MONTHS
    {
        JANUARY,
        FEBRUARY,
        MARCH,
        APRIL,
        MAY,
        JUNE,
        JULY,
        AUGUST,
        SEPTEMBER,
        OCTOBER,
        NOVEMBRE,
        DECEMBRE
    }

    private static Logger logger = LoggerFactory.getLogger(XidynTest.class);

    static
    {
        // Initialize logger.
        org.apache.log4j.BasicConfigurator.configure();
        org.apache.log4j.Logger.getRootLogger().setLevel(Level.INFO);
    }

    /**
     * Check.
     * 
     * @param title
     *            the title
     * @param source
     *            the source
     * @param model
     *            the model
     * @return the string
     */
    public static String check(final String title, final StringBuffer source, final String model)
    {
        String result;

        if (source.indexOf(model) == -1)
        {
            result = String.format("%-40s  ->  KO  <-", title) + "\nGet:\n" + source + "\nWaiting:\n" + model;

        }
        else
        {
            result = String.format("%-40s    [ OK ]  ", title);
        }

        //
        return result;
    };

    /**
     * The main method.
     * 
     * @param args
     *            the arguments
     */
    public static void main(final String[] args)
    {
        System.out.println("Automatic test action for Xid!");

        TagDataManager datas;
        SimpleTagData tag;

        String htmlSource;
        StringBuffer html;

        // Populate attributes of Test 03.
        System.out.println("----------------------------");
        datas = new TagDataManager();
        datas.setContent("name", "Superman");
        try
        {
            html = PresenterUtils.dynamize("<div id='name'>a name</div >", datas);
        }
        catch (Exception exception)
        {
            System.out.println(exception.getMessage());
            html = null;
        }

        System.out.println(check("only content change", html, "<div id=\"name\">Superman</div>"));

        // Populate attributes of Test 03.
        System.out.println("----------------------------");
        datas = new TagDataManager();
        datas.setContent("lastname", "Spiderman");
        datas.appendAttribute("lastname", "style", "background: blue;");
        datas.appendAttribute("lastname", "style", "foreground: red;");
        datas.setAttribute("lastname", "class", "nameClass");

        try
        {
            html = PresenterUtils.dynamize("<div id='lastname'>a last name</div >", datas);
        }
        catch (Exception exception)
        {
            System.out.println(exception.getMessage());
            html = null;
        }
        System.out.println(check("content and attributes", html, "<div id=\"lastname\" style=\"background: blue;foreground: red;\" class=\"nameClass\">Spiderman</div>"));

        // Populate attributes of Test 03.
        System.out.println("----------------------------");
        datas = new TagDataManager();
        datas.setContent("words", 0, "alpha");
        datas.setContent("words", 1, "bravo");
        datas.setContent("words", 2, "charlie");
        datas.setContent("words", 3, "delta");
        datas.setContent("words", 4, "echo");
        datas.setContent("words", 5, "fox");

        try
        {
            html = PresenterUtils.dynamize("<ul>\n  <li id='words'>a word</li>\n</ul>", datas);
        }
        catch (Exception exception)
        {
            System.out.println(exception.getMessage());
            html = null;
        }

        System.out.println(check("list assertion 1", html, "<li id=\"words_0\">alpha</li>"));
        System.out.println(check("list assertion 2", html, "<li id=\"words_1\">bravo</li>"));
        System.out.println(check("list assertion 3", html, "<li id=\"words_2\">charlie</li>"));
        System.out.println(check("list assertion 4", html, "<li id=\"words_3\">delta</li>"));
        System.out.println(check("list assertion 5", html, "<li id=\"words_4\">echo</li>"));
        System.out.println(check("list assertion 6", html, "<li id=\"words_5\">fox</li>"));

        // Populate attributes of Test 03.
        System.out.println("----------------------------");
        datas = new TagDataManager();
        datas.setContent("identity", 0, "nom", "Jemba");
        datas.setContent("identity", 0, "prenom", "Epo");
        datas.setContent("identity", 1, "nom", "Momon");
        datas.setContent("identity", 1, "prenom", "Christian");
        datas.setContent("identity", 2, "nom", "Tronche");
        datas.setContent("identity", 2, "prenom", "Christophe");

        StringBuffer source = new StringBuffer();
        source.append("<table>\n");
        source.append(" <tr id='identity'><td>noid</td><td id='nom'>un nom</td><td id='prenom'>un prenom</td></tr>\n");
        source.append("</table>");
        htmlSource = source.toString();
        try
        {
            html = PresenterUtils.dynamize(htmlSource, datas);
        }
        catch (Exception exception)
        {
            System.out.println(exception.getMessage());
            html = null;
        }

        System.out.println(check("table 1 assertion 1", html, "<tr id=\"identity_0\"><td>noid</td><td id=\"nom_0\">Jemba</td><td id=\"prenom_0\">Epo</td></tr>"));
        System.out.println(check("table 1 assertion 2", html, "<tr id=\"identity_1\"><td>noid</td><td id=\"nom_1\">Momon</td><td id=\"prenom_1\">Christian</td></tr>"));
        System.out.println(check("table 1 assertion 3", html, "<tr id=\"identity_2\"><td>noid</td><td id=\"nom_2\">Tronche</td><td id=\"prenom_2\">Christophe</td></tr>"));

        /*
         * // Populate attributes of Test 03. System.out.println
         * ("----------------------------"); datas = new Data ();
         * datas.setContent ("identity", 0, "nom", "Jemba"); datas.setContent
         * ("identity", 0, "prenom", "Epo"); datas.setContent ("identity", 1,
         * "nom", "Momon"); datas.setContent ("identity", 1, "prenom",
         * "Christian"); datas.setContent ("identity", 2, "nom", "Tronche");
         * datas.setContent ("identity", 2, "prenom", "Christophe");
         * datas.setIterationStrategy ("identity",
         * IdsDataByIndex.IterationStrategy.ONLY_FIRST_ROW);
         * //datas.setIterationStrategy ("identity",
         * IdsDataByIndex.IterationStrategy.ONLY_ROWS_WITH_ID);
         * //datas.setIterationStrategy ("identity",
         * IdsDataByIndex.IterationStrategy.ONLY_ROWS_WITHOUT_ID);
         * //datas.setIterationStrategy ("identity",
         * IdsDataByIndex.IterationStrategy.ALL_ROWS);
         * 
         * 
         * errorMessage = new StringBuffer (); source = new StringBuffer ();
         * source.append ("<table>\n"); source.append (
         * "  <tr id='identity'><td>noid</td><td id='nom'>un nom</td><td id='prenom'>un prenom</td></tr>\n"
         * ); source.append (
         * "  <tr id='identity2'><td>noid</td><td id='nom2'>un nom</td><td id='prenom2'>un prenom</td></tr>\n"
         * ); source.append (
         * "  <tr><td>noid</td><td id='nom3'>un nom</td><td id='prenom3'>un prenom</td></tr>\n"
         * ); source.append ("</table>"); htmlSource = source.toString ();
         * 
         * System.out.println ("datas = new Data ();"); System.out.println
         * ("datas.setContent (\"identity\", 0, \"nom\", \"Jemba\");");
         * System.out.println
         * ("datas.setContent (\"identity\", 0, \"prenom\", \"Epo\");");
         * System.out.println
         * ("datas.setContent (\"identity\", 1, \"nom\", \"Momon\");");
         * System.out.println
         * ("datas.setContent (\"identity\", 1, \"prenom\", \"Christian\");");
         * System.out.println
         * ("datas.setContent (\"identity\", 2, \"nom\", \"Tronche\");");
         * System.out.println
         * ("datas.setContent (\"identity\", 2, \"prenom\", \"Christophe\");");
         * 
         * System.out.println ("+"); System.out.println (htmlSource);
         * System.out.println ("=>");
         * 
         * 
         * datas.setIterationStrategy ("identity",
         * IdsDataByIndex.IterationStrategy.ONLY_FIRST_ROW); System.out.println
         * ("ONLY_FIRST_ROW:"); html = Presenter.dynamize (htmlSource, datas, "",
         * errorMessage); System.out.println (html);
         * 
         * datas.setIterationStrategy ("identity",
         * IdsDataByIndex.IterationStrategy.ONLY_ROWS_WITH_ID);
         * System.out.println ("ONLY_ROWS_WITH_ID:"); html = Presenter.dynamize
         * (htmlSource, datas, "", errorMessage); System.out.println (html);
         * 
         * datas.setIterationStrategy ("identity",
         * IdsDataByIndex.IterationStrategy.ONLY_ROWS_WITHOUT_ID);
         * System.out.println ("ONLY_ROWS_WITHOUT_ID:"); html = Presenter.dynamize
         * (htmlSource, datas, "", errorMessage); System.out.println (html);
         * 
         * datas.setIterationStrategy ("identity",
         * IdsDataByIndex.IterationStrategy.ALL_ROWS); System.out.println
         * ("ALL_ROWS:"); html = Presenter.dynamize (htmlSource, datas, "",
         * errorMessage); System.out.println (html);
         * 
         * 
         * 
         * // Populate attributes of Test 03. System.out.println
         * ("----------------------------"); datas = new Data ();
         * datas.setAttribute ("<div>", "class", "aDivClass");
         * datas.setAttribute ("<div>", "style", "background-color: #000000;");
         * datas.setAttribute ("number", "style", "background-color: #0000FF;");
         * 
         * errorMessage = new StringBuffer (); source = new StringBuffer ();
         * source.append ("<div>\n"); source.append ("  <h1>one</h1>\n");
         * source.append ("</div>\n"); source.append ("<div id=\"number\">\n");
         * source.append ("  <h1>three</h1>\n"); source.append ("</div>");
         * htmlSource = source.toString (); html = Presenter.dynamize (htmlSource,
         * datas, "", errorMessage);
         * 
         * System.out.println (htmlSource); System.out.println ("+");
         * System.out.println ("datas = new Data ();"); System.out.println
         * ("datas.setAttribute (\"<div>\", \"class\", \"aDivClass\");");
         * System.out.println (
         * "datas.setAttribute (\"<div>\", \"style\", \"background-color: #000000;\");"
         * ); System.out.println (
         * "datas.setAttribute (\"number\", \"style\", \"background-color: #0000FF;\");"
         * );
         * 
         * System.out.println ("=>"); System.out.println (html);
         */
    }
}
