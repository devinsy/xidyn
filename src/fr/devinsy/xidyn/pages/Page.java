/*
 * Copyright (C) 2016-2018 Christian Pierre MOMON
 * 
 * This file is part of Xidyn.
 * 
 * Xidyn is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Xidyn is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Xidyn.  If not, see <http://www.gnu.org/licenses/>
 */
package fr.devinsy.xidyn.pages;

import java.io.File;
import java.net.URL;
import java.util.Locale;

import org.w3c.dom.Document;

import fr.devinsy.xidyn.XidynException;
import fr.devinsy.xidyn.data.TagDataManager;
import fr.devinsy.xidyn.presenters.DomPresenter;
import fr.devinsy.xidyn.presenters.FilePresenter;
import fr.devinsy.xidyn.presenters.Presenter;
import fr.devinsy.xidyn.presenters.PresenterFactory;
import fr.devinsy.xidyn.presenters.URLPresenter;
import fr.devinsy.xidyn.utils.XidynUtils;

/**
 * The Class Page.
 */
public class Page extends TagDataManager
{
    private Presenter presenter;
    private StringBuffer lastDynamize;

    /**
     * Instantiates a new page.
     * 
     * @param source
     *            the source
     */
    public Page(final CharSequence source)
    {
        this(PresenterFactory.create(source));
    }

    /**
     * Instantiates a new page.
     * 
     * @param source
     *            the source
     * @param locale
     *            the locale
     */
    public Page(final CharSequence source, final Locale locale)
    {
        this(PresenterFactory.create(source, locale));
    }

    /**
     * Instantiates a new page.
     * 
     * @param source
     *            the source
     */
    public Page(final Document source)
    {
        this(new DomPresenter(source));
    }

    /**
     * Instantiates a new page.
     * 
     * @param source
     *            the source
     */
    public Page(final File source)
    {
        this(new FilePresenter(source));
    }

    /**
     * Instantiates a new page.
     * 
     * @param source
     *            the source
     * @param locale
     *            the locale
     */
    public Page(final File source, final Locale locale)
    {
        this(PresenterFactory.create(source, locale));
    }

    /**
     * Instantiates a new page.
     * 
     * @param presenter
     *            the presenter
     */
    public Page(final Presenter presenter)
    {
        super();

        if (presenter == null)
        {
            throw new IllegalArgumentException("Null parameter.");
        }
        else
        {
            this.presenter = presenter;
            this.lastDynamize = null;
        }
    }

    /**
     * Instantiates a new page.
     * 
     * @param source
     *            the source
     */
    public Page(final URL source)
    {
        this(new URLPresenter(source));
    }

    /**
     * Instantiates a new page.
     * 
     * @param source
     *            the source
     * @param locale
     *            the locale
     */
    public Page(final URL source, final Locale locale)
    {
        this(PresenterFactory.create(source, locale));
    }

    /**
     * Dynamize.
     * 
     * @return the string buffer
     * @throws XidynException
     *             the exception
     */
    public StringBuffer dynamize() throws XidynException
    {
        StringBuffer result;

        if (this.isEmpty())
        {
            this.lastDynamize = this.presenter.dynamize(null);
        }
        else
        {
            this.lastDynamize = this.presenter.dynamize(this);
        }

        result = this.lastDynamize;

        //
        return result;
    }

    /**
     * Dynamize to string.
     * 
     * @return the string
     * @throws XidynException
     *             the exception
     */
    public String dynamizeToString() throws XidynException
    {
        String result;

        StringBuffer buffer = dynamize();

        if (buffer == null)
        {
            result = null;
        }
        else
        {
            result = buffer.toString();
        }

        //
        return result;
    }

    /**
     * Include.
     * 
     * @param id
     *            the id
     * @param source
     *            the source
     * @throws XidynException
     *             the exception
     */
    public void include(final String id, final Page source) throws XidynException
    {
        if ((id != null) && (source != null))
        {
            //
            StringBuffer content = source.dynamize();

            // Extract start of content.
            String contentStart;
            if (content == null)
            {
                contentStart = null;
            }
            else if (content.length() < 7)
            {
                contentStart = content.toString();
            }
            else
            {
                contentStart = content.subSequence(0, 6).toString();
            }

            // Set content.
            String fixedContent;
            if ((contentStart != null) && ((contentStart.startsWith("<?")) || (contentStart.startsWith("<!")) || (contentStart.startsWith("<html>"))))
            {
                fixedContent = XidynUtils.extractBodyContent(content);
            }
            else
            {
                fixedContent = source.toString();
            }

            //
            setContent(id, fixedContent);
        }
    }

    /**
     * Include page.
     * 
     * @param id
     *            the id
     * @param source
     *            the source
     * @throws XidynException
     *             the exception
     */
    public void includePage(final String id, final CharSequence source) throws XidynException
    {
        if ((id != null) && (source != null))
        {
            Page page = PageFactory.instance().create(source);

            include(id, page);
        }
    }

    /**
     * Include page.
     * 
     * @param id
     *            the id
     * @param source
     *            the source
     * @throws XidynException
     *             the exception
     */
    public void includePage(final String id, final Document source) throws XidynException
    {
        if ((id != null) && (source != null))
        {
            Page page = PageFactory.instance().create(source);

            include(id, page);
        }
    }

    /**
     * Include page.
     * 
     * @param id
     *            the id
     * @param source
     *            the source
     * @throws XidynException
     *             the exception
     */
    public void includePage(final String id, final File source) throws XidynException
    {
        if ((id != null) && (source != null))
        {
            Page page = PageFactory.instance().create(source);

            include(id, page);
        }
    }

    /**
     * Include page.
     *
     * @param id
     *            the id
     * @param source
     *            the source
     * @throws XidynException
     *             the xidyn exception
     */
    public void includePage(final String id, final URL source) throws XidynException
    {
        if ((id != null) && (source != null))
        {
            Page page = PageFactory.instance().create(source);

            include(id, page);
        }
    }

    /**
     * Checks if is complete.
     * 
     * @return true, if is complete
     */
    public boolean isComplete()
    {
        boolean result;

        if (this.lastDynamize == null)
        {
            result = false;
        }
        else
        {
            result = true;
        }

        //
        return result;
    }

    /**
     * Last version.
     * 
     * @return the string buffer
     * @throws XidynException
     *             the exception
     */
    public StringBuffer lastVersion() throws XidynException
    {
        StringBuffer result;

        if ((this.lastDynamize == null) || (this.presenter.isOutdated()))
        {
            dynamize();
        }

        result = this.lastDynamize;

        //
        return result;
    }
}

// ////////////////////////////////////////////////////////////////////////