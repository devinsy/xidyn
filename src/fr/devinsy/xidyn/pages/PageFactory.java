/*
 * Copyright (C) 2016-2018 Christian Pierre MOMON
 * 
 * This file is part of Xidyn.
 * 
 * Xidyn is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Xidyn is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Xidyn.  If not, see <http://www.gnu.org/licenses/>
 */
package fr.devinsy.xidyn.pages;

import java.io.File;
import java.net.URL;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;

import fr.devinsy.strings.StringsUtils;
import fr.devinsy.xidyn.presenters.PresenterFactory;
import fr.devinsy.xidyn.utils.cache.Cache;

/**
 * A factory for creating Page objects.
 * 
 * @author Christian Pierre MOMON (christian.momon@devinsy.fr)
 */
public class PageFactory
{
    private static Logger logger = LoggerFactory.getLogger(PageFactory.class);

    /**
     * http://thecodersbreakfast.net/index.php?post/2008/02/25/26-de-la-bonne-
     * implementation-du-singleton-en-java
     * 
     */
    private static class SingletonHolder
    {
        private final static PageFactory INSTANCE = new PageFactory();
    }

    private Cache<Page> cache;

    /**
     * Instantiates a new page factory.
     */
    private PageFactory()
    {
        this.cache = new Cache<Page>();
    }

    /**
     * Clear.
     */
    public void clear()
    {
        this.cache.clear();
        PresenterFactory.instance().clear();
    }

    /**
     * Creates the.
     * 
     * @param source
     *            the source
     * @return the page
     */
    public Page create(final CharSequence source)
    {
        Page result;

        result = new Page(PresenterFactory.instance().get(source));

        //
        return result;
    }

    /**
     * Creates the.
     * 
     * @param source
     *            the source
     * @return the page
     */
    public Page create(final Document source)
    {
        Page result;

        result = new Page(PresenterFactory.instance().get(source));

        //
        return result;
    }

    /**
     * Creates the.
     * 
     * @param source
     *            the source
     * @return the page
     */
    public Page create(final File source)
    {
        Page result;

        result = new Page(PresenterFactory.instance().get(source));

        //
        return result;
    }

    /**
     * Creates the.
     * 
     * @param source
     *            the source
     * @param locale
     *            the locale
     * @return the page
     */
    public Page create(final File source, final Locale locale)
    {
        Page result;

        result = new Page(PresenterFactory.instance().get(source, locale));

        //
        return result;
    }

    /**
     * Creates the.
     * 
     * @param source
     *            the source
     * @return the page
     */
    public Page create(final URL source)
    {
        Page result;

        result = new Page(PresenterFactory.instance().get(source));

        //
        return result;
    }

    /**
     * Creates the.
     * 
     * @param source
     *            the source
     * @param locale
     *            the locale
     * @return the page
     */
    public Page create(final URL source, final Locale locale)
    {
        Page result;

        result = new Page(PresenterFactory.instance().get(source, locale));

        //
        return result;
    }

    /**
     * Gets the.
     * 
     * @param source
     *            the source
     * @param keys
     *            the keys
     * @return the page
     */
    public Page get(final Document source, final String... keys)
    {
        Page result;

        String key = StringsUtils.toStringSeparatedBy(keys, "-");

        result = this.cache.get(key);
        if (result == null)
        {
            result = create(source);
            this.cache.put(key, result);
        }

        //
        return result;
    }

    /**
     * Gets the.
     * 
     * @param source
     *            the source
     * @param keys
     *            the keys
     * @return the page
     */
    public Page get(final File source, final String... keys)
    {
        Page result;

        String key = StringsUtils.toStringSeparatedBy(keys, "-");

        result = this.cache.get(key);
        if (result == null)
        {
            result = create(source);
            this.cache.put(key, result);
        }

        //
        return result;
    }

    /**
     * Gets the.
     * 
     * @param source
     *            the source
     * @param keys
     *            the keys
     * @return the page
     */
    public Page get(final String source, final String... keys)
    {
        Page result;

        String key = StringsUtils.toStringSeparatedBy(keys, "-");

        result = this.cache.get(key);
        if (result == null)
        {
            result = create(source);
            this.cache.put(key, result);
        }

        //
        return result;
    }

    /**
     * Gets the.
     * 
     * @param source
     *            the source
     * @param keys
     *            the keys
     * @return the page
     */
    public Page get(final URL source, final String... keys)
    {
        Page result;

        String key = StringsUtils.toStringSeparatedBy(keys, "-");

        result = this.cache.get(key);
        if (result == null)
        {
            result = create(source);
            this.cache.put(key, result);
        }

        //
        return result;
    }

    /**
     * Size.
     * 
     * @return the int
     */
    public int size()
    {
        int result;

        result = this.cache.size();

        //
        return result;
    }

    /**
     * Instance.
     * 
     * @return the page factory
     */
    public static PageFactory instance()
    {
        return SingletonHolder.INSTANCE;
    }
}

// ////////////////////////////////////////////////////////////////////////