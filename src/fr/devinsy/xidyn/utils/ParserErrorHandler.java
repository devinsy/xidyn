/*
 * Copyright (C) 2006-2017 Christian Pierre MOMON
 * 
 * This file is part of Xidyn.
 * 
 * Xidyn is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Xidyn is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Xidyn.  If not, see <http://www.gnu.org/licenses/>
 */
package fr.devinsy.xidyn.utils;

import java.util.Vector;

import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXParseException;

/**
 * The Class ParserErrorHandler.
 * 
 * Extract from org.xml.sax Interface ErrorHandler:
 * 
 * "If a SAX application needs to implement customized error handling, it must
 * implement this interface and then register an instance with the XML reader
 * using the setErrorHandler method. The parser will then report all errors and
 * warnings through this interface."
 * 
 */
public class ParserErrorHandler implements ErrorHandler
{
    private Vector<String> messages;
    private int fatalErrorsCount;
    private int errorsCount;
    private int warningCount;

    /**
     * Instantiates a new parser error handler.
     */
    public ParserErrorHandler()
    {
        this.fatalErrorsCount = 0;
        this.errorsCount = 0;
        this.warningCount = 0;
        this.messages = new Vector<String>();
    }

    /**
     * All errors count.
     * 
     * @return the int
     */
    public int allErrorsCount()
    {
        int result;

        result = fatalErrorsCount() + errorsCount() + warningCount();

        //
        return result;
    }

    /**
     * Called by the XML parser to handle fatal errors.
     * 
     * @param exception
     *            the exception
     */
    @Override
    public void error(final SAXParseException exception)
    {
        String message = "Error at line " + exception.getLineNumber() + " : " + exception.getMessage();

        this.errorsCount += 1;
        this.messages.add(message);
    }

    /**
     * Errors count.
     * 
     * @return the int
     */
    public int errorsCount()
    {
        int result;

        result = this.errorsCount;

        //
        return result;
    }

    /**
     * Called by the XML parser to handle fatal errors.
     * 
     * @param exception
     *            the exception
     */
    @Override
    public void fatalError(final SAXParseException exception)
    {
        String message = "Fatal error at line " + exception.getLineNumber() + " : " + exception.getMessage();

        this.fatalErrorsCount += 1;
        this.messages.add(message);
    }

    /**
     * Fatal errors count.
     * 
     * @return the int
     */
    public int fatalErrorsCount()
    {
        int result;

        result = this.fatalErrorsCount;

        //
        return result;
    }

    /**
     * Checks for error.
     * 
     * @return true, if successful
     */
    public boolean hasError()
    {
        boolean result;

        if (allErrorsCount() == 0)
        {
            result = false;
        }
        else
        {
            result = true;
        }

        //
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        StringBuffer result;

        result = new StringBuffer();

        for (String message : this.messages)
        {
            result.append(message);
            result.append('\n');
        }

        //
        return (result.toString());
    }

    /**
     * Called by the XML parser to handle warnings.
     * 
     * @param exception
     *            the exception
     */
    @Override
    public void warning(final SAXParseException exception)
    {
        String message = "Warning at line " + exception.getLineNumber() + " : " + exception.getMessage();

        this.warningCount += 1;
        this.messages.add(message);
    }

    /**
     * Warning count.
     * 
     * @return the int
     */
    public int warningCount()
    {
        int result;

        result = this.warningCount;

        //
        return result;
    }
}
