/**
 * Copyright (C) 2016,2017 Christian Pierre MOMON
 * 
 * This file is part of Xidyn.
 * 
 * Xidyn is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Xidyn is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Xidyn.  If not, see <http://www.gnu.org/licenses/>
 */
package fr.devinsy.xidyn.utils.cache;

import java.util.HashMap;
import java.util.Map;

import fr.devinsy.xidyn.utils.cache.CacheStrategy.Strategy;

/**
 * The Class Cache.
 * 
 * @param <T>
 *            the generic type
 */
public class Cache<T>
{
    private CacheStrategy strategy;
    private Map<Object, CacheItem<T>> map;

    /**
     * Instantiates a new cache.
     */
    public Cache()
    {
        this.strategy = new CacheStrategy(Strategy.NONE, 0);
        this.map = new HashMap<Object, CacheItem<T>>();
    }

    /**
     * Instantiates a new cache.
     * 
     * @param initialCapacity
     *            the initial capacity
     */
    public Cache(final int initialCapacity)
    {
        this.strategy = new CacheStrategy(Strategy.NONE, 0);
        this.map = new HashMap<Object, CacheItem<T>>(initialCapacity);
    }

    /**
     * Clear.
     */
    public void clear()
    {
        this.map.clear();
    }

    /**
     * Gets the.
     * 
     * @param key
     *            the key
     * @return the t
     */
    public T get(final Object key)
    {
        T result;

        CacheItem<T> item = this.map.get(key);

        if (item == null)
        {
            result = null;
        }
        else
        {
            result = item.getValue();
        }

        //
        return result;
    }

    /**
     * Checks if is empty.
     * 
     * @return true, if is empty
     */
    public boolean isEmpty()
    {
        boolean result;

        result = this.map.isEmpty();

        //
        return result;
    }

    /**
     * Purge.
     */
    public void purge()
    {
        switch (this.strategy.getStrategy())
        {
            case NONE:
            {
            }
            break;

            case SIZE:
            {
                if (this.map.size() > this.strategy.getParameter())
                {
                    // TODO
                }
            }
            break;

            case TIME:
            {
                // TODO
            }
            break;
        }
    }

    /**
     * Put.
     * 
     * @param key
     *            the key
     * @param source
     *            the source
     * @return the t
     */
    public T put(final Object key, final T source)
    {
        T result;

        if (source != null)
        {
            CacheItem<T> item = this.map.get(key);

            if (item == null)
            {
                purge();
                item = new CacheItem<T>(source);
                this.map.put(key, item);
            }
            else
            {
                item.setValue(source);
            }
        }

        result = source;

        //
        return result;
    }

    /**
     * Size.
     * 
     * @return the int
     */
    public int size()
    {
        int result;

        result = this.map.size();

        //
        return result;
    }
}

// ////////////////////////////////////////////////////////////////////////