/**
 * Copyright (C) 2016,17 Christian Pierre MOMON
 * 
 * This file is part of Xidyn.
 * 
 * Xidyn is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Xidyn is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Xidyn.  If not, see <http://www.gnu.org/licenses/>
 */
package fr.devinsy.xidyn.utils.cache;

import java.util.Date;

/**
 * The Class CacheItem.
 * 
 * @param <T>
 *            the generic type
 */
public class CacheItem<T>
{
    private T value;
    private long creationDate;
    private long editionDate;
    private long accessDate;

    /**
     * Instantiates a new cache item.
     * 
     * @param source
     *            the source
     */
    public CacheItem(final T source)
    {
        this.value = source;
        this.creationDate = time();
        this.editionDate = this.creationDate;
        this.accessDate = this.creationDate;
    }

    /**
     * Access date.
     * 
     * @return the long
     */
    public long accessDate()
    {
        long result;

        result = this.accessDate;

        //
        return result;
    }

    /**
     * Creation date.
     * 
     * @return the long
     */
    public long creationDate()
    {
        long result;

        result = this.creationDate;

        //
        return result;
    }

    /**
     * Edition date.
     * 
     * @return the long
     */
    public long editionDate()
    {
        long result;

        result = this.editionDate;

        //
        return result;
    }

    /**
     * Gets the value.
     * 
     * @return the value
     */
    public T getValue()
    {
        T result;

        result = this.value;
        this.accessDate = time();

        //
        return result;
    }

    /**
     * Sets the value.
     * 
     * @param source
     *            the new value
     */
    public void setValue(final T source)
    {
        this.value = source;
        this.editionDate = time();
    }

    /**
     * Time.
     * 
     * @return the long
     */
    private static long time()
    {
        long result;

        result = new Date().getTime();

        //
        return result;
    }
}

// ////////////////////////////////////////////////////////////////////////