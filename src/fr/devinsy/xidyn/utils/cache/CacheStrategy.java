/*
 * Copyright (C) 2016,2017 Christian Pierre MOMON
 * 
 * This file is part of Xidyn.
 * 
 * Xidyn is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Xidyn is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Xidyn.  If not, see <http://www.gnu.org/licenses/>
 */
package fr.devinsy.xidyn.utils.cache;

/**
 * The Class CacheStrategy.
 */
public class CacheStrategy
{
    public enum Strategy
    {
        NONE,
        SIZE,
        TIME
    }

    private Strategy strategy;
    private long parameter;

    /**
     * Instantiates a new cache strategy.
     * 
     * @param strategy
     *            the strategy
     * @param parameter
     *            the parameter
     */
    public CacheStrategy(final Strategy strategy, final long parameter)
    {
        if (strategy == null)
        {
            throw new IllegalArgumentException("Null strategy.");
        }
        else if (parameter < 0)
        {
            throw new IllegalArgumentException("Negative parameter.");
        }
        else
        {
            this.strategy = strategy;
            this.parameter = parameter;
        }
    }

    /**
     * Gets the parameter.
     * 
     * @return the parameter
     */
    public long getParameter()
    {
        return this.parameter;
    }

    /**
     * Gets the strategy.
     * 
     * @return the strategy
     */
    public Strategy getStrategy()
    {
        return this.strategy;
    }

    /**
     * Sets the parameter.
     * 
     * @param parameter
     *            the new parameter
     */
    public void setParameter(final long parameter)
    {
        this.parameter = parameter;
    }

    /**
     * Sets the strategy.
     * 
     * @param strategy
     *            the new strategy
     */
    public void setStrategy(final Strategy strategy)
    {
        this.strategy = strategy;
    }
}

// ////////////////////////////////////////////////////////////////////////