/**
 * Copyright (C) 2006-2018 Christian Pierre MOMON
 * 
 * This file is part of Xidyn.
 * 
 * Xidyn is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Xidyn is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Xidyn.  If not, see <http://www.gnu.org/licenses/>
 */
package fr.devinsy.xidyn.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.URL;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.validation.Schema;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import fr.devinsy.xidyn.XidynException;

/**
 * The Class XidynUtils.
 */
public class XidynUtils
{
    private static final Logger logger = LoggerFactory.getLogger(XidynUtils.class);
    private static final Pattern BODY_PATTERN = Pattern.compile("(?s)^.*<[bB][oO][dD][yY]>\\s*(\\S.*\\S)\\s*</[bB][oO][dD][yY]>.*$");

    /**
     * This method adds a tag to a DOM object.
     * 
     * @param doc
     *            the doc
     * @param name
     *            the name
     * @param content
     *            the content
     */
    public static void addMetaTag(final Document doc, final String name, final String content)
    {
        // Find head tag.
        Node headNode = findHeadNode(doc);
        logger.debug("headNode = [" + headNode + "]");

        if (headNode != null)
        {
            Node metaNode = doc.createElement("meta");

            NamedNodeMap attrMap = metaNode.getAttributes();
            Node attrNode = doc.createAttribute("name");
            attrMap.setNamedItem(attrNode);
            attrNode.setNodeValue(name);

            attrNode = doc.createAttribute("content");
            attrMap.setNamedItem(attrNode);
            attrNode.setNodeValue(content);
            headNode.insertBefore(metaNode, headNode.getFirstChild());
        }
    }

    /**
     * This method builds a DOM object from a source.
     * 
     * @param source
     *            the source
     * @return the document
     * @throws XidynException
     *             the exception
     */
    public static Document buildDom(final InputStream source) throws XidynException
    {
        Document result;

        try
        {
            // Create a DocumentBuilderFactory and configure it.
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();

            // Set various configuration options.
            dbf.setValidating(false);
            dbf.setIgnoringComments(true);
            dbf.setIgnoringElementContentWhitespace(false);
            dbf.setCoalescing(false);

            // Keep entity references as they are.
            dbf.setExpandEntityReferences(false);

            // Create a DocumentBuilder that satisfies the constraints
            // specified by the DocumentBuilderFactory.
            DocumentBuilder db = dbf.newDocumentBuilder();

            ParserErrorHandler errorHandler;
            errorHandler = new ParserErrorHandler();

            // Set the error handler.
            db.setErrorHandler(errorHandler);

            Schema schema = db.getSchema();
            logger.debug("schema=" + schema);

            // Parse the input file.
            result = db.parse(source);

            if (errorHandler.hasError())
            {
                // Most time, error is (with StringPresenter):
                // "Error at line 1 : Document root element "html", must match DOCTYPE root
                // "null".
                // Error at line 1 : Document is invalid: no grammar found.
                // We ignore it. STU
                logger.debug(errorHandler.toString());
            }
        }
        catch (ParserConfigurationException exception)
        {
            String errorMessage = "Parser configuration exception: " + exception.getMessage();
            logger.error(errorMessage);
            result = null;
            throw new XidynException(errorMessage, exception);
        }
        catch (SAXException exception)
        {
            String errorMessage = "Error during SAX parsing: " + exception.getMessage();
            logger.error(errorMessage);
            result = null;
            throw new XidynException(errorMessage, exception);
        }
        catch (IOException exception)
        {
            String errorMessage = "IOError during parsing." + exception.getMessage();
            logger.error(errorMessage);
            result = null;
            throw new XidynException(errorMessage, exception);
        }

        //
        return result;
    }

    /**
     * This method builds a DOM object from a source.
     * 
     * @param source
     *            the source
     * @return the document
     * @throws XidynException
     *             the exception
     */
    public static Document buildDom(final String source) throws XidynException
    {
        Document result;

        try
        {
            // Create a DocumentBuilderFactory and configure it.
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();

            // Set various configuration options.
            dbf.setValidating(false);
            dbf.setIgnoringComments(true);
            dbf.setIgnoringElementContentWhitespace(false);
            dbf.setCoalescing(false);

            // Keep entity references as they are.
            dbf.setExpandEntityReferences(false);

            // Create a DocumentBuilder that satisfies the constraints
            // specified by the DocumentBuilderFactory.
            DocumentBuilder db = dbf.newDocumentBuilder();

            ParserErrorHandler errorHandler;
            errorHandler = new ParserErrorHandler();

            // Set the error handler.
            db.setErrorHandler(errorHandler);

            Schema schema = db.getSchema();
            logger.debug("schema=" + schema);

            // Parse the input file.
            result = db.parse(new InputSource(new StringReader(source)));

            if (errorHandler.hasError())
            {
                // Most time, error is (with StringPresenter):
                // "Error at line 1 : Document root element "html", must match DOCTYPE root
                // "null".
                // Error at line 1 : Document is invalid: no grammar found.
                // We ignore it. STU
                logger.debug(errorHandler.toString());
            }
        }
        catch (ParserConfigurationException exception)
        {
            String errorMessage = "Parser configuration exception: " + exception.getMessage();
            logger.error(errorMessage);
            result = null;
            throw new XidynException(errorMessage, exception);
        }
        catch (SAXException exception)
        {
            String errorMessage = "Error during SAX parsing: " + exception.getMessage();
            logger.error(errorMessage);
            result = null;
            throw new XidynException(errorMessage, exception);
        }
        catch (IOException exception)
        {
            String errorMessage = "IOError during parsing." + exception.getMessage();
            logger.error(errorMessage);
            result = null;
            throw new XidynException(errorMessage, exception);
        }

        //
        return result;
    }

    /**
     * Good estimation of the target length able to optimize performance.
     * 
     * @param sourceLength
     *            the source length
     * @return the int
     */
    public static int estimatedTargetLength(final long sourceLength)
    {
        int result;

        if (sourceLength < 1000)
        {
            result = (int) (sourceLength * 5);
        }
        else if (sourceLength < 50000)
        {
            result = (int) (sourceLength * 2);
        }
        else if (sourceLength < 800000)
        {
            result = (1000000);
        }
        else
        {
            result = (int) (sourceLength + 30000);
        }

        //
        return result;
    }

    /**
     * Extract body content.
     * 
     * @param source
     *            the source
     * @return the string
     */
    public static String extractBodyContent(final CharSequence source)
    {
        String result;

        if (source == null)
        {
            result = "";
        }
        else
        {
            Matcher matcher = BODY_PATTERN.matcher(source);
            if ((matcher.find()) && (matcher.groupCount() == 1))
            {
                // for (int i = 0; i <= matcher.groupCount(); i++)
                // {
                // System.out.println(i + " " + matcher.group(i));
                // }

                result = matcher.group(1);
            }
            else
            {
                result = "";
            }
        }

        //
        return result;
    }

    /**
     * This method extracts the string before the <i>html</i> tag.
     * 
     * A possible way is to use pattern searching for <i>$(.*)&lt;html&gt;.*^</i>.
     * But if there is no <i>html</i> tag, all the source is read for nothing.
     * 
     * A best way is to analyze each &lt; while it is a XML tag or a DOCTYPE tag or
     * the &lt;HTML&gt; tag.
     * 
     * Uses cases:
     * 
     * <code>
     * 		<?xml version='1.0' encoding='UTF-8' ?><!-- TOTOT --><!DOCTYPE
     * html><html xmlns='http://www.w3.org/1999/xhtml'>...
     * </code>
     * 
     * @param source
     *            the source
     * @return the string before the <i>html</i> tag or null if no <i>html</i> tag
     *         found. So, "" if there is a <i>html</i> tag without doctype.
     */
    public static String extractDoctype(final String source)
    {
        String result;

        if (source == null)
        {
            result = null;
        }
        else
        {
            boolean ended = false;
            result = null;
            int currentPosition = 0;
            while (!ended)
            {
                currentPosition = source.indexOf('<', currentPosition);

                if (currentPosition == -1)
                {
                    ended = true;
                    result = null;
                }
                else
                {
                    if (currentPosition + 1 < source.length())
                    {
                        if (Character.toLowerCase(source.charAt(currentPosition + 1)) == 'h')
                        {
                            if (source.substring(currentPosition + 1, currentPosition + 5).matches("^[hH][tT][mM][lL]$"))
                            {
                                ended = true;
                                result = source.substring(0, currentPosition);
                            }
                        }
                        else
                        {
                            char letter = source.charAt(currentPosition + 1);
                            if ((letter != '?') && (letter != '!'))
                            {
                                ended = true;
                                result = null;
                            }
                            else
                            {
                                currentPosition += 1;
                            }
                        }
                    }
                    else
                    {
                        ended = true;
                        result = null;
                    }
                }
            }
        }

        //
        return result;
    }

    /**
     * File to dom.
     * 
     * @param source
     *            the source
     * @return the document
     * @throws XidynException
     *             the exception
     */
    public static Document fileToDom(final File source) throws Exception
    {
        Document result;

        try
        {
            result = buildDom(new FileInputStream(source));
        }
        catch (IOException exception)
        {
            String errorMessage = "IOError during parsing." + exception.getMessage();
            logger.error(errorMessage);
            result = null;
            throw new Exception(errorMessage, exception);
        }

        //
        return result;
    }

    /**
     * Finds the node containing the &lt;head&gt; tag.
     * 
     * @param node
     *            Document node.
     * @return The head tag node
     */
    public static Node findHeadNode(final Node node)
    {
        Node result;

        result = findNodeByName(node, "head");

        //
        return result;
    }

    /**
     * Finds the node containing the &lt;head&gt; tag.
     * 
     * @param node
     *            Document node.
     * @param name
     *            the name
     * @return The head tag node
     */
    public static Node findNodeByName(final Node node, final String name)
    {
        Node result;

        switch (node.getNodeType())
        {
            case Node.DOCUMENT_NODE:
            {
                result = findNodeByName(((Document) node).getDocumentElement(), name);
            }
            break;

            case Node.ELEMENT_NODE:
            {
                String tag = node.getNodeName();

                if ((tag != null) && tag.equals(name))
                {
                    result = node;
                }
                else
                {
                    NodeList children = node.getChildNodes();
                    int childrenCount;
                    if (children == null)
                    {
                        childrenCount = 0;
                    }
                    else
                    {
                        childrenCount = children.getLength();
                    }

                    boolean ended = false;
                    int childrenIndex = 0;
                    result = null;
                    while (!ended)
                    {
                        if (childrenIndex < childrenCount)
                        {
                            Node subResult = findNodeByName(children.item(childrenIndex), name);
                            if (subResult == null)
                            {
                                childrenIndex += 1;
                            }
                            else
                            {
                                ended = true;
                                result = subResult;
                            }
                        }
                        else
                        {
                            ended = true;
                            result = null;
                        }
                    }
                }
            }
            break;

            default:
                result = null;
        }

        //
        return result;
    }

    /**
     * Load.
     * 
     * @param source
     *            the source
     * @return the string
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public static String load(final File source) throws IOException
    {
        String result;

        final String DEFAULT_CHARSET_NAME = "UTF-8";
        result = load(source, DEFAULT_CHARSET_NAME);

        //
        return result;
    }

    /**
     * Load.
     * 
     * @param source
     *            the source
     * @param charsetName
     *            the charset name
     * @return the string
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public static String load(final File source, final String charsetName) throws IOException
    {
        String result;

        result = loadToStringBuffer(source, charsetName).toString();

        //
        return result;
    }

    /**
     * Load.
     * 
     * @param source
     *            the source
     * @return the string
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public static String load(final URL source) throws IOException
    {
        String result;

        final String DEFAULT_CHARSET_NAME = "UTF-8";
        result = load(source, DEFAULT_CHARSET_NAME);

        //
        return result;
    }

    /**
     * Load.
     * 
     * @param source
     *            the source
     * @param charsetName
     *            the charset name
     * @return the string
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public static String load(final URL source, final String charsetName) throws IOException
    {
        String result;

        //
        StringBuffer buffer = new StringBuffer(source.openConnection().getContentLength() + 1);
        read(buffer, source.openStream(), charsetName);

        //
        result = buffer.toString();

        //
        return result;
    }

    /**
     * Load to string buffer.
     * 
     * @param file
     *            the file
     * @param charsetName
     *            the charset name
     * @return the string buffer
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public static StringBuffer loadToStringBuffer(final File file, final String charsetName) throws IOException
    {
        StringBuffer result;

        BufferedReader in = null;
        try
        {
            in = new BufferedReader(new InputStreamReader(new FileInputStream(file), charsetName));

            boolean ended = false;
            final String LINE_SEPARATOR = System.getProperty("line.separator");
            result = new StringBuffer((int) file.length() + 1);
            while (!ended)
            {
                String line = in.readLine();

                if (line == null)
                {
                    ended = true;
                }
                else
                {
                    result.append(line).append(LINE_SEPARATOR);
                }
            }
        }
        finally
        {
            try
            {
                if (in != null)
                {
                    in.close();
                }
            }
            catch (IOException exception)
            {
                exception.printStackTrace();
            }
        }

        //
        return result;
    }

    /**
     * Read.
     * 
     * @param out
     *            the out
     * @param is
     *            the is
     * @param charsetName
     *            the charset name
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public static void read(final StringBuffer out, final InputStream is, final String charsetName) throws IOException
    {
        BufferedReader in = null;
        try
        {
            in = new BufferedReader(new InputStreamReader(is, charsetName));

            boolean ended = false;
            final String LINE_SEPARATOR = System.getProperty("line.separator");

            while (!ended)
            {
                String line = in.readLine();

                if (line == null)
                {
                    ended = true;
                }
                else
                {
                    out.append(line).append(LINE_SEPARATOR);
                }
            }
        }
        finally
        {
            try
            {
                if (in != null)
                {
                    in.close();
                }
            }
            catch (IOException exception)
            {
                exception.printStackTrace();
            }
        }
    }

    /**
     * Any ampersand lt;, ampersand gt; and ampersand amp; sequences in text nodes
     * get read in as symbols. This method converts them back to entities.
     * 
     * @param source
     *            String that is to have the entities restored..
     * @return The processed string.
     */
    public static String restoreEntities(final StringBuffer source)
    {
        String result;

        if (source == null)
        {
            result = null;
        }
        else
        {
            StringBuffer buffer = new StringBuffer(source.length() * 2);

            int len = (source != null) ? source.length() : 0;
            for (int index = 0; index < len; index++)
            {
                char letter = source.charAt(index);
                switch (letter)
                {
                    case '<':
                    {
                        buffer.append("&lt;");
                        break;
                    }

                    case '>':
                    {
                        buffer.append("&gt;");
                        break;
                    }

                    case '&':
                    {
                        buffer.append("&amp;");
                        break;
                    }

                    default:
                    {
                        buffer.append(letter);
                    }
                }
            }
            result = buffer.toString();
        }

        //
        return result;
    }

    /**
     * Url to dom.
     * 
     * @param source
     *            the source
     * @return the document
     * @throws XidynException
     *             the exception
     */
    public static Document urlToDom(final URL source) throws XidynException
    {
        Document result;

        try
        {
            result = buildDom(source.openStream());
        }
        catch (IOException exception)
        {
            String errorMessage = "IOError during parsing." + exception.getMessage();
            logger.error(errorMessage);
            result = null;
            throw new XidynException(errorMessage, exception);
        }

        //
        return result;
    }
}
