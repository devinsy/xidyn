/*
 * Copyright (C) 2006-2018 Christian Pierre MOMON
 * 
 * This file is part of Xidyn.
 * 
 * Xidyn is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Xidyn is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Xidyn.  If not, see <http://www.gnu.org/licenses/>
 */
package fr.devinsy.xidyn.demo;

import org.apache.log4j.Level;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.devinsy.strings.StringList;
import fr.devinsy.xidyn.data.SimpleTagData;
import fr.devinsy.xidyn.data.TagDataManager;
import fr.devinsy.xidyn.presenters.PresenterUtils;

/**
 * The Class XidynDemo.
 */
class XidynDemo
{
    static
    {
        // Initialize logger.
        org.apache.log4j.BasicConfigurator.configure();
        org.apache.log4j.Logger.getRootLogger().setLevel(Level.INFO);
    }
    private final Logger logger = LoggerFactory.getLogger(XidynDemo.class);

    /**
     * The main method.
     * 
     * @param args
     *            the arguments
     */
    public static void main(final String[] args)
    {
        System.out.println("Hello World!");

        // Step #01.
        {
            try
            {
                System.out.println("----------------------------");

                String htmlSource = "<div id='pseudo'>a pseudo</div >";

                TagDataManager data = new TagDataManager();
                data.setContent("name", "Superman");

                String htmlTarget = PresenterUtils.dynamize(htmlSource, data).toString();

                System.out.println("datas = new Data ();");
                System.out.println("datas.setContent (\"pseudo\", \"Superman\");");
                System.out.println("+");
                System.out.println("<div id='pseudo'>a pseudo</div >");
                System.out.println("=>");
                System.out.println(htmlTarget);
                System.out.println("[" + htmlSource.length() + "] => [" + htmlTarget.length() + "]");
            }
            catch (Exception exception)
            {
                System.out.println(exception.getMessage());
            }
        }

        // Step #02.
        {
            try
            {
                System.out.println("----------------------------");

                String htmlSource = "<div id=\"pseudo\">a pseudo</div >";

                TagDataManager data = new TagDataManager();
                data.setContent("pseudo", "Spiderman");
                data.appendAttribute("pseudo", "style", "background: blue;");
                data.appendAttribute("pseudo", "style", "foreground: red;");
                data.setAttribute("pseudo", "class", "superhero");

                String htmlTarget = PresenterUtils.dynamize(htmlSource, data).toString();

                System.out.println(htmlSource);
                System.out.println("+");
                System.out.println("datas = new Data ();");
                System.out.println("datas.setContent (\"pseudo\", \"Spiderman\");");
                System.out.println("datas.appendAttribute (\"pseudo\", \"style\", \"background: blue;\");");
                System.out.println("datas.appendAttribute (\"pseudo\", \"style\", \"foreground: red;\");");
                System.out.println("datas.setAttribute (\"pseudo\", \"class\", \"superhero\");");
                System.out.println("=>");
                System.out.println(htmlTarget);
                System.out.println("[" + htmlSource.length() + "] => [" + htmlTarget.length() + "]");
            }
            catch (Exception exception)
            {
                System.out.println(exception.getMessage());
            }
        }

        // Step #03.
        {
            System.out.println("----------------------------");
            try
            {
                StringList source = new StringList();
                source.appendln("<ul>");
                source.appendln("  <li id='words'>a word</li>");
                source.appendln("</ul>");
                String htmlSource = source.toString();

                // Populate attributes.
                TagDataManager data = new TagDataManager();
                data.setContent("words", 0, "alpha");
                data.setContent("words", 1, "bravo");
                data.setContent("words", 2, "charlie");
                data.setContent("words", 3, "delta");
                data.setContent("words", 4, "echo");
                data.setContent("words", 5, "fox");

                String htmlTarget = PresenterUtils.dynamize(htmlSource, data).toString();

                System.out.println(htmlSource);
                System.out.println("+");
                System.out.println("datas = new Data ();");
                System.out.println("datas.setContent (\"words\", 0, \"alpha\");");
                System.out.println("datas.setContent (\"words\", 1, \"bravo\");");
                System.out.println("datas.setContent (\"words\", 2, \"charlie\");");
                System.out.println("datas.setContent (\"words\", 3, \"delta\");");
                System.out.println("datas.setContent (\"words\", 4, \"echo\");");
                System.out.println("datas.setContent (\"words\", 5, \"fox\");");
                System.out.println("=>");
                System.out.println(htmlTarget);
                System.out.println("[" + htmlSource.length() + "] => [" + htmlTarget.length() + "]");
            }
            catch (Exception exception)
            {
                System.out.println(exception.getMessage());
            }
        }

        // Step #04.
        {
            try
            {
                System.out.println("----------------------------");

                StringList source = new StringList();
                source.append("<table>\n");
                source.append("  <tr id='identity'><td>noid</td><td id='first_name'>Jean</td><td id='last_name'>Reve</td></tr>\n");
                source.append("</table>");
                String htmlSource = source.toString();

                // Populate attributes.
                TagDataManager data = new TagDataManager();
                data.setContent("identity", 0, "last_name", "Jemba");
                data.setContent("identity", 0, "first_name", "Epo");
                data.setContent("identity", 1, "last_name", "Momon");
                data.setContent("identity", 1, "first_name", "Christian");
                data.setContent("identity", 2, "last_name", "Tronche");
                data.setContent("identity", 2, "first_name", "Christophe");

                StringBuffer htmlTarget = PresenterUtils.dynamize(htmlSource, data);

                System.out.println(htmlSource);
                System.out.println("+");
                System.out.println("datas = new Data ();");
                System.out.println("datas.setContent (\"identity\", 0, \"last_name\", \"Jemba\");");
                System.out.println("datas.setContent (\"identity\", 0, \"first_name\", \"Epo\");");
                System.out.println("datas.setContent (\"identity\", 1, \"last_name\", \"Momon\");");
                System.out.println("datas.setContent (\"identity\", 1, \"first_name\", \"Christian\");");
                System.out.println("datas.setContent (\"identity\", 2, \"last_name\", \"Tronche\");");
                System.out.println("datas.setContent (\"identity\", 2, \"first_name\", \"Christophe\");");
                System.out.println("=>");
                System.out.println(htmlTarget);
                System.out.println("[" + htmlSource.length() + "] => [" + htmlTarget.length() + "]");
            }
            catch (Exception exception)
            {
                System.out.println(exception.getMessage());
            }
        }

        // Step #05.
        {
            System.out.println("----------------------------");

            //
            StringList source = new StringList();
            source.appendln("<ul id=\"words\">");
            source.appendln("  <li id=\"first\">alpha</li>");
            source.appendln("  <li id=\"second\">bravo</li>");
            source.appendln("  <li id=\"third\">charlie</li>");
            source.appendln("  <li>delta</li>");
            source.appendln("</ul>");
            String htmlSource = source.toString();

            // Populate attributes.
            TagDataManager data = new TagDataManager();

            System.out.println(htmlSource);
            System.out.println("+");

            // #05.1
            data.setIterationStrategy("words", SimpleTagData.IterationStrategy.ONLY_FIRST_ROW);
            System.out.println("ONLY_FIRST_ROW:");
            StringBuffer html;
            try
            {
                html = PresenterUtils.dynamize(htmlSource, data);
            }
            catch (Exception exception)
            {
                System.out.println(exception.getMessage());
                html = null;
            }
            System.out.println(html);
            System.out.println("");

            // #05.2
            data.setIterationStrategy("words", SimpleTagData.IterationStrategy.ONLY_FIRST_TWO_ROWS);
            System.out.println("ONLY_FIRST_TWO_ROWS:");
            try
            {
                html = PresenterUtils.dynamize(htmlSource, data);
            }
            catch (Exception exception)
            {
                System.out.println(exception.getMessage());
                html = null;
            }
            System.out.println(html);
            System.out.println("");

            // #05.3
            data.setIterationStrategy("words", SimpleTagData.IterationStrategy.ONLY_ROWS_WITH_ID);
            System.out.println("ONLY_ROWS_WITH_ID:");
            try
            {
                html = PresenterUtils.dynamize(htmlSource, data);
            }
            catch (Exception exception)
            {
                System.out.println(exception.getMessage());
                html = null;
            }
            System.out.println(html);
            System.out.println("");

            // #05.4
            data.setIterationStrategy("words", SimpleTagData.IterationStrategy.ONLY_ROWS_WITHOUT_ID);
            System.out.println("ONLY_ROWS_WITHOUT_ID:");
            try
            {
                html = PresenterUtils.dynamize(htmlSource, data);
            }
            catch (Exception exception)
            {
                System.out.println(exception.getMessage());
                html = null;
            }
            System.out.println(html);
            System.out.println("");

            // #05.5
            data.setIterationStrategy("words", SimpleTagData.IterationStrategy.ALL_ROWS);
            System.out.println("ALL_ROWS:");
            try
            {
                html = PresenterUtils.dynamize(htmlSource, data);
            }
            catch (Exception exception)
            {
                System.out.println(exception.getMessage());
                html = null;
            }
            System.out.println(html);
        }
    }
}
