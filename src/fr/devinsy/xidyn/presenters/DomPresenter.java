/*
 * Copyright (C) 2006-2018 Christian Pierre MOMON
 * 
 * This file is part of Xidyn.
 * 
 * Xidyn is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Xidyn is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Xidyn.  If not, see <http://www.gnu.org/licenses/>
 */
package fr.devinsy.xidyn.presenters;

import java.io.StringWriter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;

import fr.devinsy.xidyn.XidynException;
import fr.devinsy.xidyn.data.TagDataManager;
import fr.devinsy.xidyn.utils.XidynUtils;

/**
 * The Class DomPresenter.
 */
public class DomPresenter implements Presenter
{
    private static Logger logger = LoggerFactory.getLogger(DomPresenter.class);

    private Document dom;
    private boolean isOutdated;
    private StringBuffer defaultHtmlTarget;

    /**
     * Instantiates a new dom presenter.
     */
    public DomPresenter()
    {
        this.dom = null;
        this.isOutdated = false;
    }

    /**
     * Instantiates a new dom presenter.
     * 
     * @param doc
     *            the doc
     */
    public DomPresenter(final Document doc)
    {
        setSource(doc);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public StringBuffer dynamize() throws XidynException
    {
        StringBuffer result;

        if ((this.isOutdated) || (this.defaultHtmlTarget == null))
        {
            this.defaultHtmlTarget = dynamize(null);
            this.isOutdated = false;
        }

        result = this.defaultHtmlTarget;

        //
        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public StringBuffer dynamize(final TagDataManager data) throws XidynException
    {
        StringBuffer result;

        if (data == null)
        {
            result = dynamize(new TagDataManager());
        }
        else if (this.dom == null)
        {
            String errorMessage = "source not defined";
            logger.error(errorMessage);
            result = null;
            throw new XidynException(errorMessage);
        }
        else
        {
            // Build the web page.
            StringWriter writer = new StringWriter(20000);
            DomPresenterCore.dynamize(writer, this.dom, data);
            result = writer.getBuffer();
            this.isOutdated = false;
        }

        //
        return result;
    }

    /**
     * Gets the dom.
     * 
     * @return the dom
     */
    public Document getDOM()
    {
        Document result;

        result = this.dom;

        //
        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Object getSource()
    {
        Object result;

        result = this.dom;

        //
        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isAvailable()
    {
        boolean result;

        if (this.dom == null)
        {
            result = false;
        }
        else
        {
            result = true;
        }

        //
        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isOutdated() throws XidynException
    {
        boolean result;

        result = this.isOutdated;

        //
        return result;
    }

    /**
     * Sets the dom.
     * 
     * @param doc
     *            the new dom
     */
    public void setDOM(final Document doc)
    {
        this.dom = doc;
        this.isOutdated = true;
    }

    /**
     * Sets the source.
     * 
     * @param doc
     *            the new source
     */
    public void setSource(final Document doc)
    {
        this.dom = doc;
        XidynUtils.addMetaTag(this.dom, "generator", "XIDYN");
        this.isOutdated = true;
    }

    /* (non-Javadoc)
     * @see fr.devinsy.xidyn.presenters.Presenter#update()
     */
    @Override
    public void update() throws XidynException
    {

    }
}
