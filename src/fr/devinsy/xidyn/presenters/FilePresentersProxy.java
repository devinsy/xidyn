/*
 * Copyright (C) 2006-2018 Christian Pierre MOMON
 * 
 * This file is part of Xidyn.
 * 
 * Xidyn is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Xidyn is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Xidyn.  If not, see <http://www.gnu.org/licenses/>
 */
package fr.devinsy.xidyn.presenters;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.devinsy.xidyn.XidynException;

/**
 * The Class FilePresentersProxy.
 */
public class FilePresentersProxy
{
    private static Logger logger = LoggerFactory.getLogger(FilePresentersProxy.class);

    private static FilePresentersProxy instance = null;
    private FilePresenters presenters = null;

    /**
     * Instantiates a new file presenters proxy.
     * 
     * @throws XidynException
     *             the exception
     */
    protected FilePresentersProxy() throws XidynException
    {
        this.presenters = new FilePresenters();
    }

    /**
     * Gets the by source.
     * 
     * @param source
     *            the source
     * @return the by source
     * @throws XidynException
     *             the exception
     */
    public FilePresenter getBySource(final String source) throws XidynException
    {
        FilePresenter result;

        if (source == null)
        {
            result = null;
        }
        else
        {
            result = this.presenters.getBySource(source);
            if (result == null)
            {
                result = new FilePresenter(source);
                this.presenters.add(result);
            }
        }

        //
        return result;
    }

    /**
     * Instance.
     * 
     * @return the file presenters proxy
     * @throws XidynException
     *             the exception
     */
    public static FilePresentersProxy instance() throws XidynException
    {
        FilePresentersProxy result;

        if (FilePresentersProxy.instance == null)
        {
            instance = new FilePresentersProxy();
        }

        result = instance;

        //
        return result;
    }
}

// ////////////////////////////////////////////////////////////////////////