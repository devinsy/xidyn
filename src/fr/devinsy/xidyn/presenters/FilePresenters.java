/*
 * Copyright (C) 2006-2018 Christian Pierre MOMON
 * 
 * This file is part of Xidyn.
 * 
 * Xidyn is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Xidyn is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Xidyn.  If not, see <http://www.gnu.org/licenses/>
 */
package fr.devinsy.xidyn.presenters;

import java.util.Vector;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class FilePresenters.
 */
public class FilePresenters extends Vector<FilePresenter>
{
    private static final long serialVersionUID = 7058868685681354293L;
    private static Logger logger = LoggerFactory.getLogger(FilePresenters.class);

    /**
     * Instantiates a new file presenters.
     */
    public FilePresenters()
    {
        super();
    }

    /**
     * Instantiates a new file presenters.
     * 
     * @param source
     *            the source
     */
    public FilePresenters(final FilePresenters source)
    {
        super();
        for (FilePresenter presenter : source)
        {
            this.add(presenter);
        }
    }

    /**
     * Exists.
     * 
     * @param source
     *            the source
     * @return true, if successful
     */
    public boolean exists(final String source)
    {
        boolean result;

        if (this.getBySource(source) == null)
        {
            result = false;
        }
        else
        {
            result = true;
        }

        //
        return result;
    }

    /**
     * Gets the by index.
     * 
     * @param id
     *            the id
     * @return the by index
     */
    public FilePresenter getByIndex(final int id)
    {
        FilePresenter result;

        result = this.get(id);

        //
        return result;
    }

    /**
     * Gets the by source.
     * 
     * @param source
     *            the source
     * @return the by source
     */
    public FilePresenter getBySource(final String source)
    {
        FilePresenter result;

        if (source == null)
        {
            result = null;
        }
        else
        {
            result = null;
            boolean ended = false;
            int presenterIndex = 0;
            while (!ended)
            {
                if (presenterIndex < this.size())
                {
                    FilePresenter presenter = this.get(presenterIndex);

                    if (presenter.getSource().equals(source))
                    {
                        result = presenter;
                        ended = true;
                    }
                    else
                    {
                        presenterIndex += 1;
                    }
                }
                else
                {
                    ended = true;
                    result = null;
                }
            }
        }

        //
        return result;
    }

    /**
     * Gets the index of.
     * 
     * @param source
     *            the source
     * @return the index of
     */
    public int getIndexOf(final String source)
    {
        int result;

        if (source == null)
        {
            result = -1;
        }
        else
        {
            result = -1;
            boolean ended = false;
            int presenterIndex = 0;
            while (!ended)
            {
                if (presenterIndex < this.size())
                {
                    FilePresenter presenter = this.get(presenterIndex);

                    if (presenter.getSource().equals(source))
                    {
                        result = presenterIndex;
                        ended = true;
                    }
                    else
                    {
                        presenterIndex += 1;
                    }
                }
                else
                {
                    ended = true;
                    result = -1;
                }
            }
        }

        //
        return result;
    }

    /* (non-Javadoc)
     * @see java.util.Vector#toString()
     */
    @Override
    public String toString()
    {
        String result;

        result = "";

        //
        return result;
    }
}

// ////////////////////////////////////////////////////////////////////////