/*
 * Copyright (C) 2006-2018 Christian Pierre MOMON
 * 
 * This file is part of Xidyn.
 * 
 * Xidyn is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Xidyn is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Xidyn.  If not, see <http://www.gnu.org/licenses/>
 */
package fr.devinsy.xidyn.presenters;

import java.io.StringWriter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;

import fr.devinsy.xidyn.XidynException;
import fr.devinsy.xidyn.data.TagDataManager;
import fr.devinsy.xidyn.utils.XidynUtils;

/**
 * The Class StringPresenter.
 */
public class StringPresenter implements Presenter
{
    private static Logger logger = LoggerFactory.getLogger(StringPresenter.class);

    private String source;
    private String doctype;
    private Document dom;

    /**
     * Instantiates a new string presenter.
     */
    public StringPresenter()
    {
        setSource("");
    }

    /**
     * Instantiates a new string presenter.
     * 
     * @param html
     *            the html
     */
    public StringPresenter(final String html)
    {
        setSource(html);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public StringBuffer dynamize() throws XidynException
    {
        StringBuffer result;

        result = new StringBuffer(this.source);

        //
        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public StringBuffer dynamize(final TagDataManager data) throws XidynException
    {
        StringBuffer result;

        // Build the DOM.
        if (this.dom == null)
        {
            // Build the source HTML.
            String sourceHtml;
            if (this.source == null)
            {
                String errorMessage = "source not defined";
                logger.error(errorMessage);
                result = null;
                throw new XidynException(errorMessage);
            }
            else
            {
                this.doctype = XidynUtils.extractDoctype(this.source);

                if (this.doctype == null)
                {
                    StringBuffer buffer = new StringBuffer(this.source.length() + 100);
                    buffer.append("<html><head></head><body>\n");
                    buffer.append(this.source);
                    buffer.append("</body></html>");
                    sourceHtml = buffer.toString();
                }
                else
                {
                    sourceHtml = this.source;
                }
            }

            // StringBufferInputStream is deprecated so we use another solution.
            // (see
            // http://www.developpez.net/forums/archive/index.php/t-14101.html).
            this.dom = XidynUtils.buildDom(sourceHtml);
            XidynUtils.addMetaTag(this.dom, "generator", "XIDYN");
        }

        //
        if (data == null)
        {
            result = dynamize();
        }
        else
        {
            //
            StringWriter htmlCode = new StringWriter(XidynUtils.estimatedTargetLength(this.source.length()));
            if ((this.doctype != null))
            {
                htmlCode.write(this.doctype);
            }

            DomPresenterCore.dynamize(htmlCode, this.dom, data);
            StringBuffer htmlTarget = htmlCode.getBuffer();

            //
            if (htmlTarget == null)
            {
                result = null;
            }
            else if (this.doctype != null)
            {
                result = htmlTarget;
            }
            else
            {
                String bodyContent = XidynUtils.extractBodyContent(htmlTarget);

                if (bodyContent == null)
                {
                    result = null;
                }
                else
                {
                    result = new StringBuffer(bodyContent);
                }
            }
        }

        //
        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getSource()
    {
        String result;

        result = this.source;

        //
        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isAvailable()
    {
        boolean result;

        if (this.source == null)
        {
            result = false;
        }
        else
        {
            result = true;
        }

        //
        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isOutdated() throws XidynException
    {
        boolean result;

        if (this.dom == null)
        {
            result = true;
        }
        else
        {
            result = false;
        }

        //
        return result;
    }

    /**
     * Sets the source.
     * 
     * @param html
     *            the new source
     */
    public void setSource(final String html)
    {
        this.source = html;
        this.doctype = null;
        this.dom = null;
    }

    /* (non-Javadoc)
     * @see fr.devinsy.xidyn.presenters.Presenter#update()
     */
    @Override
    public void update() throws XidynException
    {
        // Nothing to do.
    }
}
