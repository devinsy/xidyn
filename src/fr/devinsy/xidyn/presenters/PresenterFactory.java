/*
 * Copyright (C) 2006-2018 Christian Pierre MOMON
 * 
 * This file is part of Xidyn.
 * 
 * Xidyn is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Xidyn is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Xidyn.  If not, see <http://www.gnu.org/licenses/>
 */
package fr.devinsy.xidyn.presenters;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;

import fr.devinsy.util.FileTools;
import fr.devinsy.xidyn.utils.cache.Cache;

/**
 * A factory for creating Presenter objects.
 */
public class PresenterFactory
{
    private static Logger logger = LoggerFactory.getLogger(PresenterFactory.class);

    /**
     * http://thecodersbreakfast.net/index.php?post/2008/02/25/26-de-la-bonne-
     * implementation-du-singleton-en-java
     */
    private static class SingletonHolder
    {
        private final static PresenterFactory INSTANCE = new PresenterFactory();
    }

    private Cache<Presenter> cache;

    /**
     * Instantiates a new presenter factory.
     */
    private PresenterFactory()
    {
        this.cache = new Cache<Presenter>();
    }

    /**
     * Clear.
     */
    public void clear()
    {
        this.cache.clear();
    }

    /**
     * Gets the.
     * 
     * @param source
     *            the source
     * @return the presenter
     */
    public Presenter get(final CharSequence source)
    {
        Presenter result;

        result = this.cache.get(source);

        if (result == null)
        {
            result = create(source);
            this.cache.put(source, result);
        }

        //
        return result;
    }

    /**
     * Gets the.
     * 
     * @param source
     *            the source
     * @param locale
     *            the locale
     * @return the presenter
     */
    public Presenter get(final CharSequence source, final Locale locale)
    {
        Presenter result;

        if (source == null)
        {
            result = null;
        }
        else if (locale == null)
        {
            result = get(source);
        }
        else
        {
            String localizedSource = FileTools.addBeforeExtension(source.toString(), "-" + locale.getLanguage());

            result = this.cache.get(localizedSource);

            if (result == null)
            {
                result = create(localizedSource);

                if ((result == null) || (!result.isAvailable()))
                {
                    result = create(source);
                }

                this.cache.put(localizedSource, result);
            }
        }

        //
        return result;
    }

    /**
     * Gets the.
     * 
     * @param source
     *            the source
     * @return the presenter
     */
    public Presenter get(final Document source)
    {
        Presenter result;

        result = this.cache.get(source);

        if (result == null)
        {
            result = create(source);
            this.cache.put(source, result);
        }

        //
        return result;
    }

    /**
     * Gets the.
     * 
     * @param source
     *            the source
     * @return the presenter
     */
    public Presenter get(final File source)
    {
        Presenter result;

        result = this.cache.get(source);

        if (result == null)
        {
            result = create(source);
            this.cache.put(source, result);
        }

        //
        return result;
    }

    /**
     * Gets the.
     * 
     * @param source
     *            the source
     * @param locale
     *            the locale
     * @return the presenter
     */
    public Presenter get(final File source, final Locale locale)
    {
        Presenter result;

        if (source == null)
        {
            result = null;
        }
        else if (locale == null)
        {
            result = get(source);
        }
        else
        {
            File localizedSource = new File(FileTools.addBeforeExtension(source.getAbsolutePath(), "-" + locale.getLanguage()));

            result = this.cache.get(localizedSource);

            if (result == null)
            {
                result = create(localizedSource);

                if ((result == null) || (!result.isAvailable()))
                {
                    result = create(source);
                }

                this.cache.put(localizedSource, result);
            }
        }

        //
        return result;
    }

    /**
     * Gets the.
     * 
     * @param source
     *            the source
     * @return the presenter
     */
    public Presenter get(final URL source)
    {
        Presenter result;

        result = this.cache.get(source);

        if (result == null)
        {
            result = create(source);
            this.cache.put(source, result);
        }

        //
        return result;
    }

    /**
     * Gets the.
     * 
     * @param source
     *            the source
     * @param locale
     *            the locale
     * @return the presenter
     */
    public Presenter get(final URL source, final Locale locale)
    {
        Presenter result;

        try
        {
            if (source == null)
            {
                result = null;
            }
            else if (locale == null)
            {
                result = get(source);
            }
            else
            {
                URL localizedSource;

                localizedSource = new URL(FileTools.addBeforeExtension(source.getPath(), "-" + locale.getLanguage()));

                result = this.cache.get(localizedSource);

                if (result == null)
                {
                    result = create(localizedSource);

                    if ((result == null) || (!result.isAvailable()))
                    {
                        result = create(source);
                    }

                    this.cache.put(localizedSource, result);
                }
            }
        }
        catch (MalformedURLException exception)
        {
            logger.error(exception.getMessage(), exception);
            result = null;
        }

        //
        return result;
    }

    /**
     * Size.
     * 
     * @return the int
     */
    public int size()
    {
        int result;

        result = this.cache.size();

        //
        return result;
    }

    /**
     * Creates the.
     * 
     * @param source
     *            the source
     * @return the presenter
     */
    public static Presenter create(final CharSequence source)
    {
        Presenter result;

        if (source == null)
        {
            result = null;
        }
        else
        {
            String target = source.toString();
            if (target.startsWith("file://"))
            {
                result = new FilePresenter(target);
            }
            else if (target.matches(".+://.+"))
            {
                result = new URLPresenter(target);
            }
            else if (target.startsWith("/"))
            {
                if (new File(target).exists())
                {
                    result = new FilePresenter(target);
                }
                else
                {
                    result = new URLPresenter(target);
                }
            }
            else
            {
                result = new StringPresenter(target);
            }
        }

        //
        return result;
    }

    /**
     * Creates the.
     * 
     * @param source
     *            the source
     * @param locale
     *            the locale
     * @return the presenter
     */
    public static Presenter create(final CharSequence source, final Locale locale)
    {
        Presenter result;

        if (source == null)
        {
            result = null;
        }
        else if (locale == null)
        {
            result = create(source);
        }
        else
        {
            String localizedSource = FileTools.addBeforeExtension(source.toString(), "-" + locale.getLanguage());

            result = create(localizedSource);

            if ((result == null) || (!result.isAvailable()))
            {
                result = create(source);
            }
        }

        //
        return result;
    }

    /**
     * Creates the.
     * 
     * @param source
     *            the source
     * @return the presenter
     */
    public static Presenter create(final Document source)
    {
        Presenter result;

        if (source == null)
        {
            result = null;
        }
        else
        {
            result = new DomPresenter(source);
        }

        //
        return result;
    }

    /**
     * Creates the.
     * 
     * @param source
     *            the source
     * @return the presenter
     */
    public static Presenter create(final File source)
    {
        Presenter result;

        if (source == null)
        {
            result = null;
        }
        else
        {
            result = new FilePresenter(source);
        }

        //
        return result;
    }

    /**
     * Creates the.
     * 
     * @param source
     *            the source
     * @param locale
     *            the locale
     * @return the presenter
     */
    public static Presenter create(final File source, final Locale locale)
    {
        Presenter result;

        if (source == null)
        {
            result = null;
        }
        else if (locale == null)
        {
            result = create(source);
        }
        else
        {
            File localizedSource = new File(FileTools.addBeforeExtension(source.getAbsolutePath(), "-" + locale.getLanguage()));

            result = create(localizedSource);

            if ((result == null) || (!result.isAvailable()))
            {
                result = create(source);
            }
        }

        //
        return result;
    }

    /**
     * Creates the.
     * 
     * @param source
     *            the source
     * @return the presenter
     */
    public static Presenter create(final URL source)
    {
        Presenter result;

        if (source == null)
        {
            result = null;
        }
        else
        {
            result = new URLPresenter(source);
        }

        //
        return result;
    }

    /**
     * Creates the.
     * 
     * @param source
     *            the source
     * @param locale
     *            the locale
     * @return the presenter
     */
    public static Presenter create(final URL source, final Locale locale)
    {
        Presenter result;

        try
        {
            if (source == null)
            {
                result = null;
            }
            else if (locale == null)
            {
                result = create(source);
            }
            else
            {
                URL localizedSource = new URL(FileTools.addBeforeExtension(source.getPath(), "-" + locale.getLanguage()));

                result = create(localizedSource);

                if ((result == null) || (!result.isAvailable()))
                {
                    result = create(source);
                }
            }
        }
        catch (MalformedURLException exception)
        {
            logger.error(exception.getMessage(), exception);
            result = null;
        }

        //
        return result;
    }

    /**
     * Instance.
     * 
     * @return the presenter factory
     */
    public static PresenterFactory instance()
    {
        return SingletonHolder.INSTANCE;
    }
}
