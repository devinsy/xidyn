/*
 * Copyright (C) 2006-2018 Christian Pierre MOMON
 * 
 * This file is part of Xidyn.
 * 
 * Xidyn is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Xidyn is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Xidyn.  If not, see <http://www.gnu.org/licenses/>
 */
package fr.devinsy.xidyn.presenters;

import java.io.File;
import java.net.URL;

import org.w3c.dom.Document;

import fr.devinsy.xidyn.XidynException;
import fr.devinsy.xidyn.data.TagDataManager;

/**
 * The Class GenericPresenter.
 */
public class GenericPresenter implements Presenter
{
    private static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(GenericPresenter.class);

    private Presenter presenter;

    /**
     * Instantiates a new generic presenter.
     * 
     * @param source
     *            the source
     */
    public GenericPresenter(final Document source)
    {
        if (source == null)
        {
            throw new NullPointerException("source is null");
        }
        else
        {
            this.presenter = PresenterFactory.create(source);
        }
    }

    /**
     * Instantiates a new generic presenter.
     * 
     * @param source
     *            the source
     */
    public GenericPresenter(final File source)
    {
        if (source == null)
        {
            throw new NullPointerException("source is null");
        }
        else
        {
            this.presenter = PresenterFactory.create(source);
        }
    }

    /**
     * Instantiates a new generic presenter.
     * 
     * @param source
     *            the source
     */
    public GenericPresenter(final String source)
    {
        if (source == null)
        {
            throw new NullPointerException("source is null");
        }
        else
        {
            this.presenter = PresenterFactory.create(source);
        }
    }

    /**
     * Instantiates a new generic presenter.
     * 
     * @param source
     *            the source
     */
    public GenericPresenter(final URL source)
    {
        if (source == null)
        {
            throw new NullPointerException("source is null");
        }
        else
        {
            this.presenter = PresenterFactory.create(source);
        }
    }

    /* (non-Javadoc)
     * @see fr.devinsy.xidyn.presenters.Presenter#dynamize()
     */
    @Override
    public StringBuffer dynamize() throws XidynException
    {
        StringBuffer result;

        result = this.presenter.dynamize();

        //
        return result;
    }

    /* (non-Javadoc)
     * @see fr.devinsy.xidyn.presenters.Presenter#dynamize(fr.devinsy.xidyn.data.TagDataManager)
     */
    @Override
    public StringBuffer dynamize(final TagDataManager datas) throws XidynException
    {
        StringBuffer result;

        result = this.presenter.dynamize(datas);

        //
        return result;
    }

    /* (non-Javadoc)
     * @see fr.devinsy.xidyn.presenters.Presenter#getSource()
     */
    @Override
    public Object getSource()
    {
        Object result;

        result = this.presenter.getSource();

        //
        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isAvailable()
    {
        boolean result;

        result = this.presenter.isAvailable();

        //
        return result;
    }

    /* (non-Javadoc)
     * @see fr.devinsy.xidyn.presenters.Presenter#isOutdated()
     */
    @Override
    public boolean isOutdated() throws XidynException
    {
        boolean result;

        result = this.presenter.isOutdated();

        //
        return result;
    }

    /**
     * Sets the source.
     * 
     * @param source
     *            the new source
     */
    public void setSource(final Document source)
    {
        if (source == null)
        {
            throw new NullPointerException("source is null");
        }
        else
        {
            this.presenter = PresenterFactory.create(source);
        }
    }

    /**
     * Sets the source.
     * 
     * @param source
     *            the new source
     */
    public void setSource(final File source)
    {
        if (source == null)
        {
            throw new NullPointerException("source is null");
        }
        else
        {
            this.presenter = PresenterFactory.create(source);
        }
    }

    /**
     * Sets the source.
     * 
     * @param source
     *            the new source
     */
    public void setSource(final String source)
    {
        if (source == null)
        {
            throw new NullPointerException("source is null");
        }
        else
        {
            this.presenter = PresenterFactory.create(source);
        }
    }

    /**
     * Sets the source.
     * 
     * @param source
     *            the new source
     */
    public void setSource(final URL source)
    {
        if (source == null)
        {
            throw new NullPointerException("source is null");
        }
        else
        {
            this.presenter = PresenterFactory.create(source);
        }
    }

    /**
     * To string.
     * 
     * @param language
     *            the language
     * @return the string
     * @throws XidynException
     *             the exception
     */
    public String toString(final String language) throws XidynException
    {
        String result;

        result = this.presenter.toString();

        //
        return result;
    }

    /* (non-Javadoc)
     * @see fr.devinsy.xidyn.presenters.Presenter#update()
     */
    @Override
    public void update() throws XidynException
    {
        this.presenter.update();
    }
}

// ////////////////////////////////////////////////////////////////////////