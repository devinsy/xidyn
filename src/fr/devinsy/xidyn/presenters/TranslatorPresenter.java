/*
 * Copyright (C) 2006-2018 Christian Pierre MOMON
 * 
 * This file is part of Xidyn.
 * 
 * Xidyn is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Xidyn is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Xidyn.  If not, see <http://www.gnu.org/licenses/>
 */
package fr.devinsy.xidyn.presenters;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import fr.devinsy.util.FileTools;
import fr.devinsy.xidyn.XidynException;
import fr.devinsy.xidyn.data.TagDataManager;

/**
 * The Class TranslatorPresenter.
 */
public class TranslatorPresenter implements Presenter
{
    private static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(TranslatorPresenter.class);
    private String defaultSource;
    private Map<String, Presenter> presenters;

    /**
     * Instantiates a new translator presenter.
     * 
     * @param defaultSource
     *            the default source
     */
    public TranslatorPresenter(final String defaultSource)
    {

        if (defaultSource == null)
        {
            throw new NullPointerException("defaultSource is null");
        }
        else
        {
            this.defaultSource = defaultSource;
            this.presenters = new HashMap<String, Presenter>();
        }
    }

    /* (non-Javadoc)
     * @see fr.devinsy.xidyn.presenters.Presenter#dynamize()
     */
    @Override
    public StringBuffer dynamize() throws XidynException
    {
        StringBuffer result;

        Presenter presenter = getPresenter((String) null);
        if (presenter == null)
        {
            result = null;
        }
        else
        {
            result = presenter.dynamize();
        }

        //
        return result;
    }

    /**
     * Dynamize.
     * 
     * @param locale
     *            the locale
     * @return the string buffer
     * @throws XidynException
     *             the exception
     */
    public StringBuffer dynamize(final Locale locale) throws XidynException
    {
        StringBuffer result;

        //
        String language;
        if (locale == null)
        {
            language = null;
        }
        else
        {
            language = locale.getLanguage();
        }

        //
        result = getPresenter(language).dynamize();

        //
        return result;
    }

    /**
     * Dynamize.
     * 
     * @param language
     *            the language
     * @return the string buffer
     * @throws XidynException
     *             the exception
     */
    public StringBuffer dynamize(final String language) throws XidynException
    {
        StringBuffer result;

        result = getPresenter(language).dynamize();

        //
        return result;
    }

    /* (non-Javadoc)
     * @see fr.devinsy.xidyn.presenters.Presenter#dynamize(fr.devinsy.xidyn.data.TagDataManager)
     */
    @Override
    public StringBuffer dynamize(final TagDataManager datas) throws XidynException
    {
        StringBuffer result;

        Presenter presenter = getPresenter((String) null);
        if (presenter == null)
        {
            result = null;
        }
        else
        {
            result = presenter.dynamize(datas);
        }

        //
        return result;
    }

    /**
     * Dynamize.
     * 
     * @param datas
     *            the datas
     * @param locale
     *            the locale
     * @return the string buffer
     * @throws XidynException
     *             the exception
     */
    public StringBuffer dynamize(final TagDataManager datas, final Locale locale) throws XidynException
    {
        StringBuffer result;

        //
        String language;
        if (locale == null)
        {
            language = null;
        }
        else
        {
            language = locale.getLanguage();
        }

        //
        result = getPresenter(language).dynamize(datas);

        //
        return result;
    }

    /**
     * Dynamize.
     * 
     * @param datas
     *            the datas
     * @param language
     *            the language
     * @return the string buffer
     * @throws XidynException
     *             the exception
     */
    public StringBuffer dynamize(final TagDataManager datas, final String language) throws XidynException
    {
        StringBuffer result;

        result = getPresenter(language).dynamize(datas);

        //
        return result;
    }

    /**
     * Gets the presenter.
     * 
     * @param locale
     *            the locale
     * @return the presenter
     * @throws XidynException
     *             the exception
     */
    public Presenter getPresenter(final Locale locale) throws XidynException
    {
        Presenter result;

        //
        String language;
        if ((locale == null) || (locale.getLanguage().length() == 0))
        {
            language = null;
        }
        else
        {
            language = locale.getLanguage();
        }

        //
        result = getPresenter(language);

        //
        return result;
    }

    /**
     * Gets the presenter.
     * 
     * @param language
     *            the language
     * @return the presenter
     * @throws XidynException
     *             the exception
     */
    public Presenter getPresenter(final String language) throws XidynException
    {
        Presenter result;

        //
        result = this.presenters.get(language);

        //
        if (result == null)
        {
            //
            if (language == null)
            {
                //
                result = PresenterFactory.create(this.defaultSource);

                if (result.isAvailable())
                {
                    this.presenters.put(language, result);
                }
                else
                {
                    throw new XidynException("Undefined default language file.");
                }
            }
            else
            {
                String adaptedSource = FileTools.addBeforeExtension(this.defaultSource, "-" + language);
                result = PresenterFactory.create(adaptedSource);

                if (result.isAvailable())
                {
                    this.presenters.put(language, result);
                }
                else
                {
                    adaptedSource = FileTools.addBeforeExtension(this.defaultSource, "_" + language);

                    if (result.isAvailable())
                    {
                        this.presenters.put(language, result);
                    }
                    else
                    {
                        result = getPresenter((String) null);
                    }
                }
            }
        }

        //
        return result;
    }

    /* (non-Javadoc)
     * @see fr.devinsy.xidyn.presenters.Presenter#getSource()
     */
    @Override
    public Object getSource()
    {
        String result;

        result = this.defaultSource;

        //
        return result;
    }

    /* (non-Javadoc)
     * @see fr.devinsy.xidyn.presenters.Presenter#isAvailable()
     */
    @Override
    public boolean isAvailable()
    {
        boolean result;

        try
        {
            result = getPresenter((String) null).isAvailable();
        }
        catch (Exception exception)
        {
            result = false;
        }

        //
        return result;
    }

    /* (non-Javadoc)
     * @see fr.devinsy.xidyn.presenters.Presenter#isOutdated()
     */
    @Override
    public boolean isOutdated() throws XidynException
    {
        return false;
    }

    /**
     * To string.
     * 
     * @param locale
     *            the locale
     * @return the string
     * @throws XidynException
     *             the exception
     */
    public String toString(final Locale locale) throws XidynException
    {
        String result;

        //
        String language;
        if (locale == null)
        {
            language = null;
        }
        else
        {
            language = locale.getLanguage();
        }

        //
        result = toString(language);

        //
        return result;
    }

    /**
     * To string.
     * 
     * @param language
     *            the language
     * @return the string
     * @throws XidynException
     *             the exception
     */
    public String toString(final String language) throws XidynException
    {
        String result;

        Presenter presenter = getPresenter(language);
        if (presenter == null)
        {
            result = null;
        }
        else
        {
            result = presenter.toString();
        }

        //
        return result;
    }

    /* (non-Javadoc)
     * @see fr.devinsy.xidyn.presenters.Presenter#update()
     */
    @Override
    public void update() throws XidynException
    {
    }
}

// ////////////////////////////////////////////////////////////////////////