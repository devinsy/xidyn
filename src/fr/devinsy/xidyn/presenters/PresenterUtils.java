/*
 * Copyright (C) 2006-2018 Christian Pierre MOMON
 * 
 * This file is part of Xidyn.
 * 
 * Xidyn is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Xidyn is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Xidyn.  If not, see <http://www.gnu.org/licenses/>
 */
package fr.devinsy.xidyn.presenters;

import java.io.File;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;

import fr.devinsy.xidyn.XidynException;
import fr.devinsy.xidyn.data.TagDataListById;
import fr.devinsy.xidyn.data.TagDataManager;

/**
 * The Class PresenterUtils.
 */
public class PresenterUtils
{
    private static Logger logger = LoggerFactory.getLogger(PresenterUtils.class);

    /**
     * Instantiates a new presenter utils.
     */
    private PresenterUtils()
    {
    }

    /**
     * Dynamize.
     * 
     * @param doc
     *            the doc
     * @param data
     *            the data
     * @return the string buffer
     * @throws XidynException
     *             the exception
     */
    public static StringBuffer dynamize(final Document doc, final TagDataListById data) throws XidynException
    {
        StringBuffer result;

        result = DomPresenterCore.dynamize(doc, data);

        //
        return result;
    }

    /**
     * Dynamize.
     * 
     * @param doc
     *            the doc
     * @param data
     *            the data
     * @return the string buffer
     * @throws XidynException
     *             the exception
     */
    public static StringBuffer dynamize(final Document doc, final TagDataManager data) throws XidynException
    {
        StringBuffer result;

        result = DomPresenterCore.dynamize(doc, data);

        //
        return result;
    }

    /**
     * Dynamize a file without data.
     * 
     * @param source
     *            the source
     * @return the string buffer
     * @throws XidynException
     *             the exception
     */
    public static StringBuffer dynamize(final File source) throws XidynException
    {
        StringBuffer result;

        FilePresenter presenter = new FilePresenter(source);

        result = presenter.dynamize((TagDataManager) null);

        //
        return result;
    }

    /**
     * Dynamize a file.
     * 
     * @param source
     *            the source
     * @param datas
     *            the datas
     * @return the string buffer
     * @throws XidynException
     *             the exception
     */
    public static StringBuffer dynamize(final File source, final TagDataManager datas) throws XidynException
    {
        StringBuffer result;

        FilePresenter presenter = new FilePresenter(source);

        result = presenter.dynamize(datas);

        //
        return result;
    }

    /**
     * Dynamize a string with HTML in, or with file path name in, or with URL in.
     * 
     * @param source
     *            the source
     * @param datas
     *            the datas
     * @return the string buffer
     * @throws XidynException
     *             the exception
     */
    public static StringBuffer dynamize(final String source, final TagDataManager datas) throws XidynException
    {
        StringBuffer result;

        Presenter presenter = PresenterFactory.create(source);

        result = presenter.dynamize(datas);

        //
        return result;
    }

    /**
     * Checks for html tag.
     * 
     * @param source
     *            the source
     * @return true, if successful
     */
    public static boolean hasHtmlTag(final String source)
    {
        boolean result;

        if (source == null)
        {
            result = false;
        }
        else
        {
            if (source.matches("<[hH][tT][mM][lL]>"))
            {
                result = true;
            }
            else
            {
                result = false;
            }
        }

        //
        return result;
    }
}
