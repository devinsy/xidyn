/*
 * Copyright (C) 2006-2018 Christian Pierre MOMON
 * 
 * This file is part of Xidyn.
 * 
 * Xidyn is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Xidyn is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Xidyn.  If not, see <http://www.gnu.org/licenses/>
 */
package fr.devinsy.xidyn.presenters;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.devinsy.xidyn.XidynException;
import fr.devinsy.xidyn.data.TagDataManager;
import fr.devinsy.xidyn.utils.XidynUtils;

/**
 * The Class URLPresenter.
 */
public class URLPresenter extends StringPresenter
{
    private static Logger logger = LoggerFactory.getLogger(URLPresenter.class);

    private String sourcePathname;
    private URL source;
    private long sourceTime;

    /**
     * Instantiates a new URL presenter.
     */
    public URLPresenter()
    {
        super();
        setSource((URL) null);
    }

    /**
     * Instantiates a new URL presenter.
     * 
     * @param source
     *            the source
     */
    public URLPresenter(final String source)
    {
        super();
        setSource(source);
    }

    /**
     * Instantiates a new URL presenter.
     * 
     * @param source
     *            the source
     */
    public URLPresenter(final URL source)
    {
        super();
        setSource(source);
    }

    /**
     * No need to be synchronized.
     * 
     * @return the string buffer
     * @throws XidynException
     *             the exception
     */
    @Override
    public StringBuffer dynamize() throws XidynException
    {
        StringBuffer result;

        //
        update();

        //
        result = super.dynamize();

        //
        return result;
    }

    /**
     * No need to be synchronized.
     * 
     * @param data
     *            the data
     * @return the string buffer
     * @throws XidynException
     *             the exception
     */
    @Override
    public StringBuffer dynamize(final TagDataManager data) throws XidynException
    {
        StringBuffer result;

        logger.info("dynamize URL [" + this.sourcePathname + "]");

        //
        update();

        // Build the web page.
        result = super.dynamize(data);

        //
        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getSource()
    {
        String result;

        result = this.sourcePathname;

        //
        return result;
    }

    /**
     * Gets the url.
     * 
     * @return the url
     */
    public URL getURL()
    {
        URL result;

        result = this.source;

        //
        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isAvailable()
    {
        boolean result;

        if (this.source == null)
        {
            result = false;
        }
        else if (this.sourcePathname.startsWith("/"))
        {
            /*
             * In case of Jar resources, if resource does not exist then
             * this.source is null. So, if we are here in the code then
             * this.source is not null and so resource is available.
             */
            result = true;
        }
        else
        {
            /*
             *  The source is an URL with protocol. Open a stream is the only way to test if the resource is available.
             */
            try
            {
                this.source.openStream();
                result = true;
            }
            catch (final IOException exception)
            {
                /*
                 *  On URL.openStream:
                 * <ul>
                 * <li>If host does not exist then a java.net.UnknownHostException is throwed.</li>
                 * <li>If host exists but not the file does not exist then a java.io.FileNotFoundException is throwed.</li>
                 * </ul>
                 * These exceptions are IOException.
                 */
                result = false;
            }
        }

        //
        return result;
    }

    /* (non-Javadoc)
     * @see fr.devinsy.xidyn.presenters.StringPresenter#isOutdated()
     */
    @Override
    public boolean isOutdated() throws XidynException
    {
        boolean result;

        try
        {
            if (super.isOutdated())
            {
                result = true;
            }
            else if (this.sourceTime == this.source.openConnection().getLastModified())
            {
                result = true;
            }
            else
            {
                result = false;
            }
        }
        catch (IOException exception)
        {
            throw new XidynException(exception);
        }

        //
        return result;
    }

    /* (non-Javadoc)
     * @see fr.devinsy.xidyn.presenters.StringPresenter#setSource(java.lang.String)
     */
    @Override
    public void setSource(final String source)
    {
        //
        if (source == null)
        {
            this.source = null;
            this.sourcePathname = null;
            this.sourceTime = 0;
            super.setSource(null);
        }
        else
        {
            //
            URL url;
            if (source.matches(".+://.+"))
            {
                try
                {
                    url = new URL(source);
                }
                catch (final MalformedURLException exception)
                {
                    // TODO
                    logger.warn("UNKNOWN PROTOCOL [" + source + "]");
                    url = null;
                }
            }
            else
            {
                url = URLPresenter.class.getResource(source);
            }

            //
            if (url == null)
            {
                this.source = null;
                this.sourcePathname = source;
                this.sourceTime = 0;
                super.setSource(null);
            }
            else
            {
                this.source = url;
                this.sourcePathname = source;
                this.sourceTime = 0;
                super.setSource(null);
            }
        }
    }

    /**
     * Sets the source.
     * 
     * @param source
     *            the new source
     */
    public void setSource(final URL source)
    {
        if (source == null)
        {
            this.source = null;
            this.sourcePathname = null;
            this.sourceTime = 0;
            super.setSource(null);
        }
        else
        {
            this.source = source;
            this.sourcePathname = source.toString();
            this.sourceTime = 0;
            super.setSource(null);
        }
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        String result;

        try
        {
            //
            update();

            //
            result = super.getSource();
        }
        catch (final Exception exception)
        {
            result = null;
        }

        //
        return result;
    }

    /**
     * No need to be synchronized.
     * 
     * @throws XidynException
     *             the exception
     */
    @Override
    public void update() throws XidynException
    {
        //
        if (this.source == null)
        {
            String errorMessage = "source not defined (" + this.sourcePathname + ")";
            logger.error(errorMessage);
            throw new NullPointerException(errorMessage);
        }
        else
        {
            try
            {
                long currentSourceTime = this.source.openConnection().getLastModified();
                if ((super.getSource() == null) || (this.sourceTime != currentSourceTime))
                {
                    super.setSource(XidynUtils.load(this.source));
                    this.sourceTime = currentSourceTime;
                }
            }
            catch (final IOException exception)
            {
                /* On URL.openStream:
                 * <ul>
                 * <li>If host does not exist then a java.net.UnknownHostException is throwed.</li>
                 * <li>If host exists but not the file does not exist then a java.io.FileNotFoundException is throwed.</li>
                 * </ul>
                 * These exceptions are IOException.
                 */
                String errorMessage = "source file defined but not readable (" + this.source.toString() + ")";
                logger.error(errorMessage);
                throw new XidynException(errorMessage);
            }
        }
    }
}
