/*
 * Copyright (C) 2006-2018 Christian Pierre MOMON
 * 
 * This file is part of Xidyn.
 * 
 * Xidyn is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Xidyn is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Xidyn.  If not, see <http://www.gnu.org/licenses/>
 */
package fr.devinsy.xidyn.presenters;

import java.io.IOException;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.DocumentType;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import fr.devinsy.xidyn.XidynException;
import fr.devinsy.xidyn.data.SimpleTagData;
import fr.devinsy.xidyn.data.TagAttributes;
import fr.devinsy.xidyn.data.TagData;
import fr.devinsy.xidyn.data.TagDataListById;
import fr.devinsy.xidyn.data.TagDataListByIndex;
import fr.devinsy.xidyn.data.TagDataManager;
import fr.devinsy.xidyn.utils.XidynUtils;

/**
 * The Class DomPresenterCore.
 */
public class DomPresenterCore
{
    private static Logger logger = LoggerFactory.getLogger(DomPresenterCore.class);

    public static final char INDEX_SEPARATOR = '_';
    public static final String NODISPLAY_CLASS = "xid:nodisplay";

    /**
     * Dynamize a doc with data.
     * 
     * @param result
     *            the result
     * @param doc
     *            the doc
     * @param data
     *            the data
     * @throws XidynException
     *             the exception
     */
    public static void dynamize(final Appendable result, final Document doc, final TagDataListById data) throws XidynException
    {
        try
        {
            process(result, doc, data);
        }
        catch (IOException exception)
        {
            throw new XidynException("IO Error detected: " + exception.getMessage(), exception);
        }
    }

    /**
     * Dynamize a doc with data.
     * 
     * @param result
     *            the result
     * @param doc
     *            the doc
     * @param data
     *            the data
     * @throws XidynException
     *             the exception
     */
    public static void dynamize(final Appendable result, final Document doc, final TagDataManager data) throws XidynException
    {
        dynamize(result, doc, data.getIdsDataById());
    }

    /**
     * Dynamize a doc with data.
     * 
     * @param doc
     *            the doc
     * @param data
     *            the data
     * @return the string buffer
     * @throws XidynException
     *             the exception
     */
    public static StringBuffer dynamize(final Document doc, final TagDataListById data) throws XidynException
    {
        StringBuffer result;

        StringWriter htmlCode = new StringWriter();
        dynamize(htmlCode, doc, data);
        result = htmlCode.getBuffer();

        //
        return result;
    }

    /**
     * Dynamize a doc with data.
     * 
     * @param doc
     *            the doc
     * @param data
     *            the data
     * @return the string buffer
     * @throws XidynException
     *             the exception
     */
    public static StringBuffer dynamize(final Document doc, final TagDataManager data) throws XidynException
    {
        StringBuffer result;

        result = dynamize(doc, data.getIdsDataById());

        //
        return result;
    }

    /**
     * Gets the class attribute value.
     * 
     * @param node
     *            the node
     * @return the class attribute value
     */
    private static String getClassAttributeValue(final Node node)
    {
        String result;

        NamedNodeMap attributes = node.getAttributes();
        if (attributes == null)
        {
            result = null;
        }
        else
        {
            Node nameAttribute = attributes.getNamedItem("class");

            if (nameAttribute == null)
            {
                result = null;
            }
            else
            {
                result = nameAttribute.getNodeValue();
            }
        }

        //
        return result;
    }

    /**
     * Get the text for an element. Converts new lines to spaces.
     * 
     * @param node
     *            the node
     * @return the element text
     */
    private static String getElementText(final Node node)
    {
        String result;

        NodeList children = node.getChildNodes();
        if ((children == null) || (children.getLength() == 0))
        {
            result = "";
        }
        else
        {
            boolean ended = false;
            result = ""; // Grrrr, Java ...
            int childCounter = 0;
            int childCount = children.getLength();
            while (!ended)
            {
                if (childCounter >= childCount)
                {
                    ended = true;
                    result = "";
                }
                else
                {
                    Node child = children.item(childCounter);
                    if (child.getNodeType() == Node.TEXT_NODE)
                    {
                        // STU (+=, newLines...)
                        result = newLinesToSpaces(child.getNodeValue());
                        ended = true;
                    }
                    else
                    {
                        childCounter += 1;
                    }
                }
            }
        }

        //
        return result;
    }

    /**
     * Merge attributes.
     * 
     * @param target
     *            the target
     * @param source
     *            the source
     * @return the tag attributes
     */
    private static TagAttributes mergeAttributes(final TagAttributes target, final TagAttributes source)
    {
        TagAttributes result;

        //
        if (target == null)
        {
            result = source;
        }
        else if (source == null)
        {
            result = target;
        }
        else
        {
            result = new TagAttributes(target);

            Iterator<Map.Entry<String, String>> iterator = source.entrySet().iterator();

            while (iterator.hasNext())
            {
                Map.Entry<String, String> attribute = iterator.next();

                String currentValue = target.get(attribute.getKey());

                if (currentValue == null)
                {
                    result.put(attribute.getKey(), attribute.getValue());
                }
                else if (attribute.getKey().equals("style"))
                {
                    result.put(attribute.getKey(), currentValue + attribute.getValue());
                }
                else
                {
                    result.put(attribute.getKey(), attribute.getValue());
                }
            }
        }

        //
        return result;
    }

    /**
     * Converts New Line characters to spaces. This is used when for example the
     * text in a div tag goes over several lines.
     * 
     * @param text
     *            String
     * @return String
     */
    private static String newLinesToSpaces(final String text)
    {
        StringBuffer result = new StringBuffer(text);

        for (int i = 0; i < result.length(); i++)
        {
            if (result.charAt(i) == '\n')
            {
                result.setCharAt(i, ' ');
            }
        }

        //
        return (result.toString());
    }

    /**
     * Process.
     * 
     * @param result
     *            the result
     * @param node
     *            the node
     * @param data
     *            the data
     * @throws IOException
     *             the exception
     */
    private static void process(final Appendable result, final Node node, final TagDataListById data) throws IOException
    {
        process(result, node, data, "");
    }

    /**
     * Recursive method that processes a node and any child nodes.
     * 
     * @param result
     *            the result
     * @param node
     *            the node
     * @param datas
     *            the datas
     * @param suffix
     *            the suffix
     * @throws IOException
     *             the exception
     */
    private static void process(final Appendable result, final Node node, final TagDataListById datas, final String suffix) throws IOException
    {
        logger.debug("process - started");
        String TRANSITIONAL_DTD = "xhtml1-transitional.dtd";
        String TRANSITIONAL_DOCTYPE = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 " + "Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n";

        // Is there anything to do?
        if (node != null)
        {
            logger.debug("nodeName=[{}]", node.getNodeName());
            // Find the name attribute value.
            String name;
            name = getClassAttributeValue(node);

            if ((name == null) || ((name != null) && (!name.equals(NODISPLAY_CLASS))))
            {
                int type = node.getNodeType();
                switch (type)
                {
                    case Node.DOCUMENT_NODE:
                    {
                        logger.debug("case Node.DOCUMENT_NODE");
                        DocumentType dt = ((Document) node).getDoctype();

                        if (dt != null)
                        {
                            // String publicId = dt.getPublicId();
                            String systemId = dt.getSystemId();

                            if ((systemId != null) && (systemId.equals(TRANSITIONAL_DTD)))
                            {
                                result.append(TRANSITIONAL_DOCTYPE);
                            }

                            // Log.write(Log.TRACE,"publicId = " + publicId);
                            // Log.write(Log.TRACE,"systemId = " + systemId);
                        }

                        process(result, ((Document) node).getDocumentElement(), datas, suffix);

                        break;
                    }

                    case Node.ELEMENT_NODE:
                    {
                        logger.debug("case Node.ELEMENT_NODE [{}]", node.getNodeName());

                        NamedNodeMap attrs = node.getAttributes();
                        Node idAttr = attrs.getNamedItem("id");

                        if (idAttr != null)
                        {
                            processElementWithId(result, node, attrs, idAttr, datas, suffix);
                        }
                        else
                        {
                            processElementBasically(result, node, datas, suffix);
                        }

                        break;
                    }

                    // handle entity reference nodes
                    case Node.ENTITY_REFERENCE_NODE:
                    {
                        logger.debug("case Node.ENTITY_REFERENCE_NODE");

                        result.append('&');
                        result.append(node.getNodeName());
                        result.append(';');
                        break;
                    }

                    // print cdata sections
                    case Node.CDATA_SECTION_NODE:
                    {
                        logger.debug("case Node.CDATA_SECTION_NODE");

                        result.append("<![CDATA[");
                        result.append(node.getNodeValue());
                        result.append("]]>");

                        break;
                    }

                    // print text
                    case Node.TEXT_NODE:
                    {
                        logger.debug("case Node.TEXTE_NODE");
                        result.append(XidynUtils.restoreEntities(new StringBuffer(node.getNodeValue())));
                        break;
                    }

                    // print processing instruction
                    case Node.PROCESSING_INSTRUCTION_NODE:
                    {
                        logger.debug("Node.PROCESSING_INSTRUCTION_NODE");

                        result.append("<?");
                        result.append(node.getNodeName());
                        String value = node.getNodeValue();
                        if ((value != null) && (value.length() > 0))
                        {
                            result.append(' ');
                            result.append(value);
                        }
                        result.append("?>");
                        break;
                    }
                }
            }
        }

        //
        // logger.info ("result=" + result);
        logger.debug("process - ended");
    }

    /**
     * Process attributes.
     * 
     * @param attrs
     *            the attrs
     * @return the string buffer
     */
    private static StringBuffer processAttributes(final NamedNodeMap attrs)
    {
        StringBuffer result;

        result = processAttributes(attrs, null, null, "");

        //
        return result;
    }

    /**
     * Process attributes.
     * 
     * @param attrs
     *            the attrs
     * @param dataAttributes
     *            the data attributes
     * @return the string buffer
     */
    private static StringBuffer processAttributes(final NamedNodeMap attrs, final TagAttributes dataAttributes)
    {
        StringBuffer result;

        result = processAttributes(attrs, dataAttributes, "");

        //
        return result;
    }

    /**
     * Process attributes.
     * 
     * @param attrs
     *            the attrs
     * @param dataAttributes
     *            the data attributes
     * @param suffix
     *            the suffix
     * @return the string buffer
     */
    private static StringBuffer processAttributes(final NamedNodeMap attrs, final TagAttributes dataAttributes, final String suffix)
    {
        StringBuffer result;

        result = new StringBuffer();

        // Build the original attributes list.
        HashMap<String, String> mergedAttributes;
        mergedAttributes = new HashMap<String, String>();
        for (int attributeCounter = 0; attributeCounter < attrs.getLength(); attributeCounter++)
        {
            Attr attr = (Attr) attrs.item(attributeCounter);
            mergedAttributes.put(attr.getNodeName(), attr.getNodeValue());
        }

        // Put model attributes in the merged attributes list.
        if (dataAttributes != null)
        {
            Iterator<Map.Entry<String, String>> iterator = dataAttributes.entrySet().iterator();

            while (iterator.hasNext())
            {
                Map.Entry<String, String> attribute = iterator.next();

                if (mergedAttributes.containsKey(attribute.getKey()))
                {
                    if (attribute.getKey().equalsIgnoreCase("style"))
                    {
                        mergedAttributes.put(attribute.getKey(), mergedAttributes.get(attribute.getKey()) + attribute.getValue());
                    }
                    else
                    {
                        mergedAttributes.put(attribute.getKey(), attribute.getValue());
                    }
                }
                else
                {
                    mergedAttributes.put(attribute.getKey(), attribute.getValue());
                }
            }
        }

        // Display the attributes
        Iterator<Map.Entry<String, String>> iterator = mergedAttributes.entrySet().iterator();
        while (iterator.hasNext())
        {
            Map.Entry<String, String> attribute = iterator.next();

            if ((attribute.getKey().equals("id")) && (suffix.length() != 0))
            {
                result.append(" " + attribute.getKey() + "=\"" + attribute.getValue() + INDEX_SEPARATOR + suffix + "\"");
            }
            else
            {
                result.append(" " + attribute.getKey() + "=\"" + attribute.getValue() + "\"");
            }
        }

        //
        return result;
    }

    /**
     * Process attributes.
     * 
     * @param attrs
     *            the attrs
     * @param dataAttributes
     *            the data attributes
     * @param namedDataAttributes
     *            the named data attributes
     * @param suffix
     *            the suffix
     * @return the string buffer
     */
    private static StringBuffer processAttributes(final NamedNodeMap attrs, final TagAttributes dataAttributes, final TagAttributes namedDataAttributes, final String suffix)
    {
        StringBuffer result;

        result = processAttributes(attrs, mergeAttributes(dataAttributes, namedDataAttributes), suffix);

        //
        return result;
    }

    /**
     * Process children.
     * 
     * @param result
     *            the result
     * @param node
     *            the node
     * @param datas
     *            the datas
     * @throws IOException
     *             the exception
     */
    private static void processChildren(final Appendable result, final Node node, final TagDataListById datas) throws IOException
    {
        processChildren(result, node, datas, "");
    }

    /**
     * Process children.
     * 
     * @param result
     *            the result
     * @param node
     *            the node
     * @param datas
     *            the datas
     * @param suffix
     *            the suffix
     * @throws IOException
     *             the exception
     */
    private static void processChildren(final Appendable result, final Node node, final TagDataListById datas, final String suffix) throws IOException
    {
        // Get the iteration strategy.
        SimpleTagData.IterationStrategy strategy;

        NamedNodeMap attributes = node.getAttributes();
        if (attributes == null)
        {
            strategy = SimpleTagData.IterationStrategy.ALL_ROWS;
        }
        else
        {
            Node id = attributes.getNamedItem("id");

            if (id == null)
            {
                strategy = SimpleTagData.IterationStrategy.ALL_ROWS;
            }
            else
            {
                TagData dataCore = datas.getId(id.getNodeValue());
                if (dataCore == null)
                {
                    strategy = SimpleTagData.IterationStrategy.ALL_ROWS;
                }
                else if (dataCore instanceof SimpleTagData)
                {
                    SimpleTagData data = (SimpleTagData) dataCore;
                    strategy = data.iterationStrategy();
                }
                else
                {
                    strategy = SimpleTagData.IterationStrategy.ALL_ROWS;
                }
            }
        }

        // Iterate.
        NodeList children = node.getChildNodes();
        int childrenCount = children.getLength();

        switch (strategy)
        {
            case ONLY_FIRST_ROW:
                int lineCounter = 0;
                for (int childIndex = 0; childIndex < childrenCount; childIndex++)
                {
                    if (children.item(childIndex).getNodeType() == Node.ELEMENT_NODE)
                    {
                        lineCounter += 1;
                        if (lineCounter == 1)
                        {
                            process(result, children.item(childIndex), datas, suffix);
                        }
                    }
                    else
                    {
                        process(result, children.item(childIndex), datas, suffix);
                    }
                }
            break;

            case ONLY_FIRST_TWO_ROWS:
                lineCounter = 0;
                for (int childIndex = 0; childIndex < childrenCount; childIndex++)
                {
                    if (children.item(childIndex).getNodeType() == Node.ELEMENT_NODE)
                    {
                        lineCounter += 1;

                        if ((lineCounter == 1) || (lineCounter == 2))
                        {
                            process(result, children.item(childIndex), datas, suffix);
                        }
                    }
                    else
                    {
                        process(result, children.item(childIndex), datas, suffix);
                    }
                }
            break;

            case ONLY_ROWS_WITH_ID:
                for (int childIndex = 0; childIndex < childrenCount; childIndex++)
                {
                    if (children.item(childIndex).getNodeType() == Node.ELEMENT_NODE)
                    {
                        NamedNodeMap attrs2 = children.item(childIndex).getAttributes();

                        if ((attrs2 != null) && (attrs2.getNamedItem("id") != null))
                        {
                            process(result, children.item(childIndex), datas, suffix);
                        }
                    }
                    else
                    {
                        process(result, children.item(childIndex), datas, suffix);
                    }
                }
            break;

            case ONLY_ROWS_WITHOUT_ID:
                for (int childIndex = 0; childIndex < childrenCount; childIndex++)
                {
                    if (children.item(childIndex).getNodeType() == Node.ELEMENT_NODE)
                    {
                        NamedNodeMap attrs2 = children.item(childIndex).getAttributes();
                        if ((attrs2 == null) || (attrs2.getNamedItem("id") == null))
                        {
                            process(result, children.item(childIndex), datas, suffix);
                        }
                    }
                    else
                    {
                        process(result, children.item(childIndex), datas, suffix);
                    }
                }
            break;

            case ALL_ROWS:
                for (int childIndex = 0; childIndex < childrenCount; childIndex++)
                {
                    process(result, children.item(childIndex), datas, suffix);
                }
            break;
        }
    }

    /**
     * TODO remove?.
     * 
     * @param result
     *            the result
     * @param node
     *            the node
     * @param datas
     *            the datas
     * @throws IOException
     *             the exception
     */
    private static void processElementBasically(final Appendable result, final Node node, final TagDataListById datas) throws IOException
    {
        processElementBasically(result, node, datas, "");
    }

    /**
     * Process element basically.
     * 
     * @param result
     *            the result
     * @param node
     *            the node
     * @param datas
     *            the datas
     * @param suffix
     *            the suffix
     * @throws IOException
     *             the exception
     */
    private static void processElementBasically(final Appendable result, final Node node, final TagDataListById datas, final String suffix) throws IOException
    {
        logger.debug("processElementBasically - started [{}]", node.getNodeName());

        // Open the tag.
        result.append('<');
        result.append(node.getNodeName());

        // Build the tag attributes.
        // Attributes tagAttributes;

        result.append(processAttributes(node.getAttributes(), null, suffix));

        //
        if ((node.getChildNodes() == null) || (node.getChildNodes().getLength() == 0))
        {
            result.append(" />");
        }
        else
        {
            result.append('>');

            processChildren(result, node, datas, suffix);

            result.append("</");
            result.append(node.getNodeName());
            result.append('>');
        }

        logger.debug("processElementBasically - ended");
    }

    /**
     * Processes a node that has dynamic content. Calls the appropriate code
     * generator method, depending on the tag.
     * 
     * @param result
     *            the result
     * @param node
     *            Current node.
     * @param attrs
     *            The tag attributes.
     * @param idAttr
     *            The ID.
     * @param datas
     *            the datas
     * @throws IOException
     *             the exception
     */
    private static void processElementWithId(final Appendable result, final Node node, final NamedNodeMap attrs, final Node idAttr, final TagDataListById datas) throws IOException
    {
        processElementWithId(result, node, attrs, idAttr, datas, "");
    }

    /**
     * Processes a node that has dynamic content. Calls the appropriate code
     * generator method, depending on the tag.
     * 
     * @param result
     *            the result
     * @param node
     *            Current node.
     * @param attrs
     *            The tag attributes.
     * @param idAttr
     *            The ID.
     * @param datas
     *            the datas
     * @param suffix
     *            the suffix
     * @throws IOException
     *             the exception
     */
    private static void processElementWithId(final Appendable result, final Node node, final NamedNodeMap attrs, final Node idAttr, final TagDataListById datas, final String suffix) throws IOException
    {
        String tag = node.getNodeName();

        // String idValue = idAttr.getNodeValue();

        logger.debug("tag=" + tag);

        // Get data of this id.
        TagData dataCore = datas.get(idAttr.getNodeValue());

        if (dataCore == null)
        {
            processElementBasically(result, node, datas, suffix);
        }
        else if (dataCore instanceof SimpleTagData)
        {
            SimpleTagData data = (SimpleTagData) dataCore;

            String theClass = data.attributes().getAttribute("class");

            if ((theClass == null) || (!theClass.equals(NODISPLAY_CLASS)))
            {
                // Open the tag.
                result.append("<");
                result.append(node.getNodeName());

                // Build attributes.
                result.append(processAttributes(attrs, data.attributes(), suffix));

                if (((node.getChildNodes() == null) || (node.getChildNodes().getLength() == 0)) && ((data == null) || (data.display() == null)))
                {
                    // Close the tag.
                    result.append(" />");
                }
                else
                {
                    result.append('>');

                    // CHANGED, cpm:

                    // Insert data.
                    if ((data == null) || (data.display() == null))
                    {
                        processChildren(result, node, datas, suffix);
                    }
                    else
                    {
                        result.append(data.display());
                    }

                    // Close the tag.
                    result.append("</");
                    result.append(node.getNodeName());
                    result.append('>');
                }
            }
        }
        else if (dataCore instanceof TagDataListByIndex)
        {
            TagDataListByIndex tags = (TagDataListByIndex) dataCore;

            int nbLines = tags.size();
            for (int nLine = 0; nLine < nbLines; nLine++)
            {
                if (tags.elementAt(nLine) instanceof SimpleTagData)
                {
                    SimpleTagData data = (SimpleTagData) tags.elementAt(nLine);

                    // Open the tag.
                    result.append("<");
                    result.append(node.getNodeName());

                    result.append(processAttributes(attrs, data.attributes(), Integer.toString(nLine)));

                    if (((node.getChildNodes() == null) || (node.getChildNodes().getLength() == 0)) && ((data == null) || (data.display() == null)))
                    {
                        // Close the tag.
                        result.append(" />\n");
                    }
                    else
                    {
                        result.append('>');

                        // CHANGED, cpm

                        // Insert data.
                        if ((data == null) || (data.display() == null))
                        {
                            processChildren(result, node, datas, suffix);
                        }
                        else
                        {
                            result.append(data.display());
                        }

                        // Close the tag.
                        result.append("</");
                        result.append(node.getNodeName());
                        result.append(">\n");
                    }
                }
                else
                {
                    // Manage a Hashmap.
                    TagDataListById data = (TagDataListById) tags.elementAt(nLine);

                    processElementWithId(result, node, attrs, idAttr, data, Integer.toString(nLine));
                    result.append('\n');
                }
            }
        }
        else
        {
            logger.warn("Unknow type of IdDataId");
        }

        //
        logger.debug("Exit");
    }
}
