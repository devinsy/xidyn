/*
 * Copyright (C) 2006-2018 Christian Pierre MOMON
 * 
 * This file is part of Xidyn.
 * 
 * Xidyn is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Xidyn is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Xidyn.  If not, see <http://www.gnu.org/licenses/>
 */
package fr.devinsy.xidyn.presenters;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.devinsy.xidyn.XidynException;
import fr.devinsy.xidyn.data.TagDataManager;
import fr.devinsy.xidyn.utils.XidynUtils;

/**
 * The Class FilePresenter.
 */
public class FilePresenter extends StringPresenter
{
    private static Logger logger = LoggerFactory.getLogger(FilePresenter.class);

    private File source;
    private String sourceFilePathname;
    private long sourceTime;

    /**
     * Instantiates a new file presenter.
     */
    public FilePresenter()
    {
        setSource((String) null);
    }

    /**
     * Instantiates a new file presenter.
     * 
     * @param source
     *            the source
     */
    public FilePresenter(final File source)
    {
        setSource(source);
    }

    /**
     * Instantiates a new file presenter.
     * 
     * @param filePathname
     *            the file pathname
     */
    public FilePresenter(final String filePathname)
    {
        setSource(filePathname);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public StringBuffer dynamize() throws XidynException
    {
        StringBuffer result;

        //
        update();

        //
        result = super.dynamize();

        //
        return result;
    }

    /**
     * No need to be synchronized.
     * 
     * {@inheritDoc}
     */
    @Override
    public StringBuffer dynamize(final TagDataManager data) throws XidynException
    {
        StringBuffer result;

        logger.info("dynamize file [" + this.sourceFilePathname + "]");

        //
        update();

        // Build the web page.
        result = super.dynamize(data);

        //
        return result;
    }

    /**
     * Gets the file.
     * 
     * @return the file
     */
    public File getFile()
    {
        File result;

        result = this.source;

        //
        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getSource()
    {
        String result;

        result = this.sourceFilePathname;

        //
        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isAvailable()
    {
        boolean result;

        if ((this.source == null) || (!this.source.exists()))
        {
            result = false;
        }
        else
        {
            result = true;
        }

        //
        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isOutdated() throws XidynException
    {
        boolean result;

        //
        if (super.isOutdated())
        {
            result = true;
        }
        else if (this.sourceTime == this.source.lastModified())
        {
            result = true;
        }
        else
        {
            result = false;
        }

        //
        return result;
    }

    /**
     * Sets the source.
     * 
     * @param source
     *            the new source
     */
    public void setSource(final File source)
    {
        if (source == null)
        {
            super.setSource(null);
            this.source = null;
            this.sourceFilePathname = null;
            this.sourceTime = 0;
            setSource((String) null);
        }
        else
        {
            this.source = source;
            this.sourceFilePathname = source.getAbsolutePath();
            this.sourceTime = 0;
            super.setSource(null);
        }
    }

    /**
     * Sets the source.
     * 
     * @param source
     *            the new source
     */
    @Override
    public void setSource(final String source)
    {
        //
        File file;
        if (source == null)
        {
            file = null;
        }
        else if (source.matches("file://.+"))
        {
            try
            {
                file = new File(new URI(source));
            }
            catch (URISyntaxException exception)
            {
                exception.printStackTrace();
                throw new IllegalArgumentException("Bad URI argument.", exception);
            }
        }
        else
        {
            file = new File(source);
        }

        //
        setSource(file);
    }

    /* (non-Javadoc)
     * @see fr.devinsy.xidyn.presenters.StringPresenter#update()
     */
    @Override
    public void update() throws XidynException
    {
        try
        {
            //
            if (this.source == null)
            {
                String errorMessage = "source not defined";
                logger.error(errorMessage);
                throw new NullPointerException(errorMessage);
            }
            else if (!this.source.exists())
            {
                String errorMessage = "source file defined but not found (" + this.sourceFilePathname + ")";
                logger.error(errorMessage);
                throw new XidynException(errorMessage);
            }
            else
            {
                long currentTime = this.source.lastModified();
                if ((super.getSource() == null) || (this.sourceTime != currentTime))
                {
                    super.setSource(XidynUtils.load(this.source));
                    this.sourceTime = currentTime;
                }
            }
        }
        catch (IOException exception)
        {
            throw new XidynException(exception);
        }
    }
}
