/*
 * Copyright (C) 2006-2018 Christian Pierre MOMON
 * 
 * This file is part of Xidyn.
 * 
 * Xidyn is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Xidyn is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Xidyn.  If not, see <http://www.gnu.org/licenses/>
 */
package fr.devinsy.xidyn.presenters;

import fr.devinsy.xidyn.XidynException;
import fr.devinsy.xidyn.data.TagDataManager;

/**
 * The Interface Presenter.
 * 
 * This objectives of a presenter are:
 * <ul>
 * <li>return the untouched source if there is no tag data (<i>dynamize()</i>)
 * <li>detect when source is touched
 * <li>be aware to avoid copy action (performance)
 * </ul>
 */
public interface Presenter
{
    /**
     * Dynamize.
     * 
     * @return the string buffer
     * @throws XidynException
     *             the exception
     */
    public StringBuffer dynamize() throws XidynException;

    /**
     * Dynamize.
     * 
     * @param datas
     *            the datas
     * @return the string buffer
     * @throws XidynException
     *             the exception
     */
    public StringBuffer dynamize(final TagDataManager datas) throws XidynException;

    /**
     * Gets the source.
     * 
     * @return the source
     */
    public Object getSource();

    /**
     * This method check if the source exists and it is readable.
     * 
     * @return true, if is available
     */
    public boolean isAvailable();

    /**
     * Checks if is outdated.
     * 
     * @return true, if is outdated
     * @throws XidynException
     *             the exception
     */
    public boolean isOutdated() throws XidynException;

    /**
     * Update.
     * 
     * @throws XidynException
     *             the exception
     */
    public void update() throws XidynException;
}
