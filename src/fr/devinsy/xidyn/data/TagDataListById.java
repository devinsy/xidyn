/*
 * Copyright (C) 2006-2017 Christian Pierre MOMON
 * 
 * This file is part of Xidyn.
 * 
 * Xidyn is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Xidyn is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Xidyn.  If not, see <http://www.gnu.org/licenses/>
 */
package fr.devinsy.xidyn.data;

import java.util.HashMap;

/**
 * The Class TagDataListById.
 */
public class TagDataListById extends HashMap<String, TagData> implements TagData
{
    private static final long serialVersionUID = -5787252043825503554L;

    /**
     * Instantiates a new tag data list by id.
     */
    public TagDataListById()
    {
        super();
    }

    /**
     * Gets the id.
     * 
     * @param id
     *            the id
     * @return the id
     */
    public TagData getId(final String id)
    {
        TagData result;

        result = this.get(id);

        //
        return result;
    }

    /**
     * Sets the id.
     * 
     * @param id
     *            the id
     * @param data
     *            the data
     */
    public void setId(final String id, final TagData data)
    {
        this.put(id, data);
    }
}
