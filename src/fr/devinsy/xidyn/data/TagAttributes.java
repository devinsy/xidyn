/*
 * Copyright (C) 2006-2017 Christian Pierre MOMON
 * 
 * This file is part of Xidyn.
 * 
 * Xidyn is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Xidyn is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Xidyn.  If not, see <http://www.gnu.org/licenses/>
 */
package fr.devinsy.xidyn.data;

import java.util.HashMap;

/**
 * The Class TagAttributes.
 * 
 * Note: no more AttrValue as in Brill, because the exception of style is
 * managed in the attribute merging on the "style" string detection.
 */

public class TagAttributes extends HashMap<String, String>
{
    private static final long serialVersionUID = 2802739066295665336L;

    /**
     * 
     */
    public TagAttributes()
    {
        super();
    }

    /**
     * Instantiates a new tag attributes. Useful for the merge of attributes.
     *
     * @param attributes
     *            the attributes
     */
    public TagAttributes(final TagAttributes attributes)
    {
        super(attributes);
    }

    /**
     * Add a value to an existing value. This is useful to the 'style' attribute.
     * 
     * @param label
     *            the label
     * @param value
     *            the value
     */
    public void appendAttribute(final String label, final String value)
    {
        if (this.containsKey(label))
        {
            this.put(label, this.get(label) + " " + value);
        }
        else
        {
            this.put(label, value);
        }
    }

    /**
     * Gets the attribute.
     *
     * @param label
     *            the label
     * @return the attribute
     */
    public String getAttribute(final String label)
    {
        String result;

        if (this.containsKey(label))
        {
            result = this.get(label);
        }
        else
        {
            result = null;
        }

        //
        return result;
    }

    /**
     * Sets the attribute.
     *
     * @param label
     *            the label
     * @param value
     *            the value
     */
    public void setAttribute(final String label, final String value)
    {
        this.put(label, value);
    }
}
