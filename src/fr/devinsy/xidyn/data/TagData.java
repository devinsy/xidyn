/*
 * Copyright (C) 2006-2017 Christian Pierre MOMON
 * 
 * This file is part of Xidyn.
 * 
 * Xidyn is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Xidyn is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Xidyn.  If not, see <http://www.gnu.org/licenses/>
 */
package fr.devinsy.xidyn.data;

/**
 * The Interface TagData.
 * 
 * Xidyn uses three classes to describe data:
 * <ul>
 * <li>TagData</li>
 * <li>TagsData</li>
 * <li>TagsDataById</li>
 * </ul>
 * <br/>
 * Others classes that do not extends these won't be use by Xidyn.
 * 
 * This interface helps to express this fact.
 */
public interface TagData
{
}
