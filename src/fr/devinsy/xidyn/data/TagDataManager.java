/*
 * Copyright (C) 2006-2017 Christian Pierre MOMON
 * 
 * This file is part of Xidyn.
 * 
 * Xidyn is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Xidyn is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Xidyn.  If not, see <http://www.gnu.org/licenses/>
 */
package fr.devinsy.xidyn.data;

import fr.devinsy.xidyn.presenters.DomPresenterCore;

/**
 * The Class TagDataManager.
 * 
 * @author Christian Pierre MOMON (christian.momon@devinsy.fr)
 */
public class TagDataManager
{
    private TagDataListById idsDataById;

    /**
     * Instantiates a new tag data manager.
     */
    public TagDataManager()
    {
        this.idsDataById = new TagDataListById();
    }

    /**
     * Adds the content.
     * 
     * @param id
     *            the id
     * @param value
     *            the value
     */
    public void addContent(final String id, final long value)
    {
        addContent(id, String.valueOf(value));
    }

    /**
     * Adds the content.
     * 
     * @param id
     *            the id
     * @param content
     *            the content
     */
    public void addContent(final String id, final String content)
    {
        TagDataListByIndex tags = (TagDataListByIndex) this.idsDataById.getId(id);

        if (tags == null)
        {
            tags = new TagDataListByIndex();
            this.idsDataById.setId(id, tags);
        }

        // Be sure that lines are existing.
        SimpleTagData tag = new SimpleTagData();
        tag.setContent(content);
        tags.add(tag);
    }

    /**
     * Append attribute.
     * 
     * @param id
     *            the id
     * @param label
     *            the label
     * @param value
     *            the value
     */
    public void appendAttribute(final String id, final String label, final long value)
    {
        appendAttribute(id, label, String.valueOf(value));
    }

    /**
     * Append attribute.
     * 
     * @param id
     *            the id
     * @param label
     *            the label
     * @param value
     *            the value
     */
    public void appendAttribute(final String id, final String label, final String value)
    {
        SimpleTagData tag = this.getIdData(id);

        tag.attributes().appendAttribute(label, value);
    }

    /**
     * Append content.
     * 
     * @param id
     *            the id
     * @param line
     *            the line
     * @param value
     *            the value
     */
    public void appendContent(final String id, final int line, final long value)
    {
        appendContent(id, line, String.valueOf(value));
    }

    /**
     * Append content.
     * 
     * @param id
     *            the id
     * @param line
     *            the line
     * @param value
     *            the value
     */
    public void appendContent(final String id, final int line, final String value)
    {
        SimpleTagData tag = this.getIdData(id, line);

        tag.appendContent(value);
    }

    /**
     * Append content.
     * 
     * @param id
     *            the id
     * @param line
     *            the line
     * @param column
     *            the column
     * @param value
     *            the value
     */
    public void appendContent(final String id, final int line, final String column, final long value)
    {
        appendContent(id, line, column, String.valueOf(value));
    }

    /**
     * Append content.
     * 
     * @param id
     *            the id
     * @param line
     *            the line
     * @param column
     *            the column
     * @param value
     *            the value
     */
    public void appendContent(final String id, final int line, final String column, final String value)
    {
        SimpleTagData tag = this.getIdData(id, line, column);

        tag.appendContent(value);
    }

    /**
     * Append content.
     * 
     * @param id
     *            the id
     * @param value
     *            the value
     */
    public void appendContent(final String id, final String value)
    {
        SimpleTagData tag = this.getIdData(id);
        tag.appendContent(value);
    }

    /**
     * Gets the id data.
     * 
     * @param id
     *            the id
     * @return the id data
     */
    public SimpleTagData getIdData(final String id)
    {
        SimpleTagData result;

        // Be sure that IdData is existing and get item.
        result = (SimpleTagData) this.idsDataById.getId(id);

        if (result == null)
        {
            this.idsDataById.setId(id, new SimpleTagData());

            result = (SimpleTagData) this.idsDataById.getId(id);
        }

        //
        return result;
    }

    /**
     * Gets the id data.
     * 
     * @param id
     *            the id
     * @param line
     *            the line
     * @return the id data
     */
    public SimpleTagData getIdData(final String id, final int line)
    {
        SimpleTagData result;

        // Be sure that IdsData are existing.
        TagDataListByIndex tags = (TagDataListByIndex) this.idsDataById.getId(id);
        if (tags == null)
        {
            this.idsDataById.setId(id, new TagDataListByIndex());

            tags = (TagDataListByIndex) this.idsDataById.getId(id);
        }

        // Be sure that lines are existing.
        int lineCount = tags.size();
        for (int lineIndex = lineCount; lineIndex < line + 1; lineIndex++)
        {
            tags.add(lineIndex, new SimpleTagData());
        }

        // Get item.
        result = (SimpleTagData) tags.elementAt(line);

        //
        return result;
    }

    /**
     * Gets the id data.
     * 
     * @param id
     *            the id
     * @param line
     *            the line
     * @param column
     *            the column
     * @return the id data
     */
    public SimpleTagData getIdData(final String id, final int line, final String column)
    {
        SimpleTagData result;

        // Be sure that IdsData are existing.
        TagDataListByIndex tags = (TagDataListByIndex) this.idsDataById.getId(id);
        if (tags == null)
        {
            this.idsDataById.setId(id, new TagDataListByIndex());

            tags = (TagDataListByIndex) this.idsDataById.getId(id);
        }

        // Be sure that lines are existing.
        int nbLines = tags.size();
        for (int nLine = nbLines; nLine < line + 1; nLine++)
        {
            tags.add(nLine, new TagDataListById());
        }

        // Get item.
        TagDataListById lineData = (TagDataListById) tags.elementAt(line);

        result = (SimpleTagData) lineData.get(column);

        if (result == null)
        {
            lineData.put(column, new SimpleTagData());

            result = (SimpleTagData) lineData.get(column);
        }

        //
        return result;
    }

    /**
     * Gets the ids data by id.
     * 
     * @return the ids data by id
     */
    public TagDataListById getIdsDataById()
    {
        TagDataListById result;

        result = this.idsDataById;

        //
        return result;
    }

    /**
     * Checks if is empty.
     * 
     * @return true, if is empty
     */
    public boolean isEmpty()
    {
        boolean result;

        if (this.idsDataById.isEmpty())
        {
            result = true;
        }
        else
        {
            result = false;
        }

        //
        return result;
    }

    /**
     * Removes the tag.
     * 
     * @param id
     *            the id
     */
    public void removeTag(final String id)
    {
        SimpleTagData tag = this.getIdData(id);

        tag.attributes().setAttribute("class", DomPresenterCore.NODISPLAY_CLASS);
    }

    /**
     * Sets the attribute.
     * 
     * @param id
     *            the id
     * @param line
     *            the line
     * @param label
     *            the label
     * @param value
     *            the value
     */
    public void setAttribute(final String id, final int line, final String label, final long value)
    {
        setAttribute(id, line, label, String.valueOf(value));
    }

    /**
     * Sets the attribute.
     * 
     * @param id
     *            the id
     * @param line
     *            the line
     * @param label
     *            the label
     * @param value
     *            the value
     */
    public void setAttribute(final String id, final int line, final String label, final String value)
    {
        SimpleTagData tag = this.getIdData(id, line);

        tag.attributes().setAttribute(label, value);
    }

    /**
     * Sets the attribute.
     * 
     * @param id
     *            the id
     * @param line
     *            the line
     * @param column
     *            the column
     * @param label
     *            the label
     * @param value
     *            the value
     */
    public void setAttribute(final String id, final int line, final String column, final String label, final long value)
    {
        setAttribute(id, line, column, label, String.valueOf(value));
    }

    /**
     * Sets the attribute.
     * 
     * @param id
     *            the id
     * @param line
     *            the line
     * @param column
     *            the column
     * @param label
     *            the label
     * @param value
     *            the value
     */
    public void setAttribute(final String id, final int line, final String column, final String label, final String value)
    {
        SimpleTagData tag = this.getIdData(id, line, column);

        tag.attributes().setAttribute(label, value);
    }

    /**
     * Sets the attribute.
     * 
     * @param id
     *            the id
     * @param label
     *            the label
     * @param value
     *            the value
     */
    public void setAttribute(final String id, final String label, final long value)
    {
        setAttribute(id, label, String.valueOf(value));
    }

    /**
     * Sets the attribute.
     * 
     * @param id
     *            the id
     * @param label
     *            the label
     * @param value
     *            the value
     */
    public void setAttribute(final String id, final String label, final String value)
    {
        SimpleTagData tag = this.getIdData(id);

        tag.attributes().setAttribute(label, value);
    }

    /**
     * Sets the content.
     * 
     * @param id
     *            the id
     * @param line
     *            the line
     * @param value
     *            the value
     */
    public void setContent(final String id, final int line, final long value)
    {
        setContent(id, line, String.valueOf(value));
    }

    /**
     * Sets the content.
     * 
     * @param id
     *            the id
     * @param line
     *            the line
     * @param content
     *            the content
     */
    public void setContent(final String id, final int line, final String content)
    {
        SimpleTagData tag = this.getIdData(id, line);

        tag.setContent(content);
    }

    /**
     * Sets the content.
     * 
     * @param id
     *            the id
     * @param line
     *            the line
     * @param subId
     *            the sub id
     * @param subLine
     *            the sub line
     * @param column
     *            the column
     * @param content
     *            the content
     * @TODO
     */
    public void setContent(final String id, final int line, final String subId, final int subLine, final String column, final String content)
    {
        // IdData tag = this.getIdData (id, line, subId, subLine, column);

        // tag.setContent (content);
    }

    /**
     * Sets the content.
     * 
     * @param id
     *            the id
     * @param line
     *            the line
     * @param column
     *            the column
     * @param value
     *            the value
     */
    public void setContent(final String id, final int line, final String column, final long value)
    {
        setContent(id, line, column, String.valueOf(value));
    }

    /**
     * Sets the content.
     * 
     * @param id
     *            the id
     * @param line
     *            the line
     * @param column
     *            the column
     * @param content
     *            the content
     */
    public void setContent(final String id, final int line, final String column, final String content)
    {
        SimpleTagData tag = this.getIdData(id, line, column);

        tag.setContent(content);
    }

    /**
     * Sets the content.
     * 
     * @param id
     *            the id
     * @param value
     *            the value
     */
    public void setContent(final String id, final long value)
    {
        setContent(id, String.valueOf(value));
    }

    /**
     * Sets the content.
     * 
     * @param id
     *            the id
     * @param content
     *            the content
     */
    public void setContent(final String id, final String content)
    {
        SimpleTagData idData = this.getIdData(id);

        idData.setContent(content);
    }

    /**
     * Sets the iteration strategy.
     * 
     * @param id
     *            the id
     * @param strategy
     *            the strategy
     */
    public void setIterationStrategy(final String id, final SimpleTagData.IterationStrategy strategy)
    {
        SimpleTagData tag = this.getIdData(id);

        tag.setIterationStrategy(strategy);
    }
}
