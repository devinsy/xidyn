/*
 * Copyright (C) 2006-2017 Christian Pierre MOMON
 * 
 * This file is part of Xidyn.
 * 
 * Xidyn is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Xidyn is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Xidyn.  If not, see <http://www.gnu.org/licenses/>
 */
package fr.devinsy.xidyn.data;

import java.util.Vector;

/**
 * The Class TagDataListByIndex.
 * 
 * @author Christian Pierre MOMON (christian.momon@devinsy.fr)
 */
public class TagDataListByIndex extends Vector<TagData> implements TagData
{
    private static final long serialVersionUID = 215545720925753884L;

    /**
     * Instantiates a new tag data list by index.
     */
    public TagDataListByIndex()
    {
        super();
    }
}
