/*
 * Copyright (C) 2006-2017 Christian Pierre MOMON
 * 
 * This file is part of Xidyn.
 * 
 * Xidyn is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Xidyn is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Xidyn.  If not, see <http://www.gnu.org/licenses/>
 */
package fr.devinsy.xidyn.data;

import java.io.Serializable;

/**
 * The Class SimpleTagData.
 * 
 * IdData class is used to hold application data and the business logic that
 * operates on the data.
 * 
 * The only requirement of a IdData class is that it must implement a display
 * method. The display method must return a text representation of the data,
 * suitable for display in a web page.
 * 
 * XID provides a User Input IdData, Text IdData and ... application may also
 * implement it's own IdData classes.
 * 
 */
public class SimpleTagData implements Serializable, TagData
{
    public enum IterationStrategy
    {
        ONLY_FIRST_ROW,
        ONLY_FIRST_TWO_ROWS,
        ONLY_ROWS_WITH_ID,
        ONLY_ROWS_WITHOUT_ID,
        ALL_ROWS
    }

    public enum MODE
    {
        REPLACE,
        APPEND,
        IGNORE
    }

    private static final long serialVersionUID = 8976245034682639923L;;

    private IterationStrategy iterationStrategy;
    private TagAttributes attributes;
    private boolean excludeSection;
    private MODE displayMode = MODE.REPLACE;
    private String content;

    /**
     * Instantiates a new simple tag data.
     */
    public SimpleTagData()
    {
        this.attributes = null;
        this.excludeSection = false;
        this.displayMode = MODE.REPLACE;
        this.content = null;
        this.iterationStrategy = IterationStrategy.ALL_ROWS;
    }

    /**
     * Instantiates a new simple tag data.
     * 
     * @param text
     *            the text
     */
    public SimpleTagData(final String text)
    {
        this.attributes = null;
        this.excludeSection = false;
        this.displayMode = MODE.REPLACE;
        this.content = text;
        this.iterationStrategy = IterationStrategy.ALL_ROWS;
    }

    /**
     * Append content.
     * 
     * @param text
     *            the text
     */
    public void appendContent(final String text)
    {
        if (this.content == null)
        {
            this.content = text;
        }
        else
        {
            this.content += text;
        }
    }

    /**
     * Attributes.
     * 
     * @return the tag attributes
     */
    public TagAttributes attributes()
    {
        TagAttributes result;

        if (this.attributes == null)
        {
            this.attributes = new TagAttributes();
        }

        result = this.attributes;

        //
        return result;
    }

    /**
     * Display.
     * 
     * @return the string
     */
    public String display()
    {
        String result;

        result = this.content;

        //
        return result;
    }

    /**
     * Display mode.
     * 
     * @return the mode
     */
    public MODE displayMode()
    {
        MODE result;

        result = this.displayMode;

        return result;
    }

    /**
     * Exclude section.
     * 
     * @return true, if successful
     */
    public boolean excludeSection()
    {
        boolean result;

        result = this.excludeSection;

        //
        return result;
    }

    /**
     * Iteration strategy.
     * 
     * @return the iteration strategy
     */
    public IterationStrategy iterationStrategy()
    {
        IterationStrategy result;

        result = this.iterationStrategy;

        //
        return result;
    }

    /**
     * Sets the content.
     * 
     * @param text
     *            the new content
     */
    public void setContent(final String text)
    {
        this.content = text;
    }

    /**
     * Sets the display mode.
     * 
     * @param displayMode
     *            the new display mode
     */
    public void setDisplayMode(final MODE displayMode)
    {
        this.displayMode = displayMode;
    }

    /**
     * Sets the exclude section.
     * 
     * @param excludeSection
     *            the new exclude section
     */
    public void setExcludeSection(final boolean excludeSection)
    {
        this.excludeSection = excludeSection;
    }

    /**
     * Sets the iteration strategy.
     * 
     * @param strategy
     *            the new iteration strategy
     */
    public void setIterationStrategy(final IterationStrategy strategy)
    {
        this.iterationStrategy = strategy;
    }
}
