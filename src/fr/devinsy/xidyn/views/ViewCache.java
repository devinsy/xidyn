/*
 * Copyright (C) 2006-2017 Christian Pierre MOMON
 * 
 * This file is part of Xidyn.
 * 
 * Xidyn is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Xidyn is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Xidyn.  If not, see <http://www.gnu.org/licenses/>
 */
package fr.devinsy.xidyn.views;

import java.util.HashMap;
import java.util.Map;

import fr.devinsy.xidyn.XidynException;

/**
 * The Class ViewCache.
 */
public class ViewCache
{
    private Map<String, ViewCacheItem> cache;

    /**
     * Instantiates a new view cache.
     */
    public ViewCache()
    {
        this.cache = new HashMap<String, ViewCacheItem>();
    }

    /**
     * Gets the HT ml.
     * 
     * @param key
     *            the key
     * @return the HT ml
     * @throws XidynException
     *             the exception
     */
    public String getHTMl(final String key) throws XidynException
    {
        String result;

        ViewCacheItem item = this.cache.get(key);
        if (item == null)
        {
            result = null;
        }
        else
        {
            if (item.isDeprecated())
            {
                item.update();
            }

            result = item.view().getHtml().toString();
        }

        //
        return result;
    }

    /**
     * Gets the view.
     * 
     * @param key
     *            the key
     * @return the view
     */
    public View getView(final String key)
    {
        View result;

        ViewCacheItem item = this.cache.get(key);
        if (item == null)
        {
            result = null;
        }
        else
        {
            result = item.view();
        }

        //
        return result;
    }

    /**
     * Checks if is empty.
     * 
     * @return true, if is empty
     */
    public boolean isEmpty()
    {
        boolean result;

        result = this.cache.isEmpty();

        //
        return result;
    }

    /**
     * Purge.
     */
    public void purge()
    {
        for (ViewCacheItem item : this.cache.values())
        {

        }
    }

    /**
     * Put.
     * 
     * @param view
     *            the view
     * @param key
     *            the key
     * @return the string
     */
    public String put(final View view, final String key)
    {
        String result;

        ViewCacheItem item = new ViewCacheItem(view);
        this.cache.put(key, item);

        //
        result = item.html().toString();

        //
        return result;
    }
}

// ////////////////////////////////////////////////////////////////////////