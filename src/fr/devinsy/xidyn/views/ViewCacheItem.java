/*
 * Copyright (C) 2006-2017 Christian Pierre MOMON
 * 
 * This file is part of Xidyn.
 * 
 * Xidyn is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Xidyn is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Xidyn.  If not, see <http://www.gnu.org/licenses/>
 */
package fr.devinsy.xidyn.views;

import java.util.Date;

/**
 * The Class ViewCacheItem.
 */
public class ViewCacheItem
{
    private View view;
    private long creationDate;
    private long editionDate;
    private long lastUseDate;
    private StringBuffer html;

    /**
     * Instantiates a new view cache item.
     * 
     * @param source
     *            the source
     */
    public ViewCacheItem(final View source)
    {
        this.view = source;
        this.creationDate = time();
        update();
        this.editionDate = this.creationDate;
        this.lastUseDate = this.creationDate;
    }

    /**
     * Creation date.
     * 
     * @return the long
     */
    public long creationDate()
    {
        long result;

        result = this.creationDate;

        //
        return result;
    }

    /**
     * Edition date.
     * 
     * @return the long
     */
    public long editionDate()
    {
        long result;

        result = this.editionDate;

        //
        return result;
    }

    /**
     * Html.
     * 
     * @return the string buffer
     */
    public StringBuffer html()
    {
        return this.html;
    }

    /**
     * Checks if is deprecated.
     * 
     * @return true, if is deprecated
     */
    public boolean isDeprecated()
    {
        boolean result;

        if ((this.view == null) || (this.html == null))
        {
            result = true;
        }
        else
        {
            // TODO
            // result = this.view.isOutdated();
            result = false;
        }

        //
        return result;
    }

    /**
     * Last use date.
     * 
     * @return the long
     */
    public long lastUseDate()
    {
        long result;

        result = this.lastUseDate;

        //
        return result;
    }

    /**
     * Last use time.
     * 
     * @return the long
     */
    public long lastUseTime()
    {
        return this.lastUseDate;
    }

    /**
     * Update.
     */
    public void update()
    {
        this.editionDate = time();
        this.lastUseDate = this.editionDate;
        // TODO
        // this.html = this.view.getHtml();
    }

    /**
     * View.
     * 
     * @return the view
     */
    public View view()
    {
        return this.view;
    }

    /**
     * Time.
     * 
     * @return the long
     */
    public static long time()
    {
        long result;

        result = new Date().getTime();

        //
        return result;
    }
}

// ////////////////////////////////////////////////////////////////////////