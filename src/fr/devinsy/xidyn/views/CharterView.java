/*
 * Copyright (C) 2006-2017 Christian Pierre MOMON
 * 
 * This file is part of Xidyn.
 * 
 * Xidyn is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Xidyn is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Xidyn.  If not, see <http://www.gnu.org/licenses/>
 */
package fr.devinsy.xidyn.views;

import java.util.Locale;

import fr.devinsy.xidyn.XidynException;

/**
 * The Interface CharterView.
 */
public interface CharterView extends View
{
    /**
     * Gets the html.
     * 
     * @param userId
     *            the user id
     * @param language
     *            the language
     * @param content
     *            the content
     * @return the html
     * @throws XidynException
     *             the exception
     */
    public StringBuffer getHtml(final Long userId, final Locale language, final CharSequence content) throws XidynException;
}

// ////////////////////////////////////////////////////////////////////////