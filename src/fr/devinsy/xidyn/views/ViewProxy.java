/*
 * Copyright (C) 2006-2017 Christian Pierre MOMON
 * 
 * This file is part of Xidyn.
 * 
 * Xidyn is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Xidyn is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Xidyn.  If not, see <http://www.gnu.org/licenses/>
 */
package fr.devinsy.xidyn.views;

import java.io.Serializable;

/**
 * The Class ViewProxy.
 */
public class ViewProxy implements Serializable
{
    /**
	 * 
	 */
    private static class Holder
    {
        private final static ViewProxy INSTANCE = new ViewProxy();
    }

    private static final long serialVersionUID = -5517300613792689510L;

    /**
     * Instance.
     * 
     * @return the view proxy
     */
    public ViewProxy instance()
    {
        return Holder.INSTANCE;
    }

    /**
     * http://thecodersbreakfast.net/index.php?post/2008/02/25/26-de-la-bonne-
     * implementation-du-singleton-en-java
     * 
     * @return the object
     */
    private Object readResolve()
    {
        return Holder.INSTANCE;
    }
}

// ////////////////////////////////////////////////////////////////////////