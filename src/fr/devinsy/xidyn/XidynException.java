/*
 * Copyright (C) 2016,2017 Christian Pierre MOMON
 * 
 * This file is part of Xidyn.
 * 
 * Xidyn is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Xidyn is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Xidyn.  If not, see <http://www.gnu.org/licenses/>
 */
package fr.devinsy.xidyn;

/**
 * The Class XidynException.
 * 
 * @author Christian Pierre MOMON (christian.momon@devinsy.fr)
 */
public class XidynException extends Exception
{
    private static final long serialVersionUID = 8498031594322380559L;

    /**
     * Instantiates a new xidyn exception.
     */
    public XidynException()
    {
        super();
    }

    /**
     * Instantiates a new xidyn exception.
     * 
     * @param message
     *            the message
     */
    public XidynException(final String message)
    {
        super(message);
    }

    /**
     * Instantiates a new xidyn exception.
     * 
     * @param message
     *            the message
     * @param cause
     *            the cause
     */
    public XidynException(final String message, final Throwable cause)
    {
        super(message, cause);
    }

    /**
     * Instantiates a new xidyn exception.
     * 
     * @param cause
     *            the cause
     */
    public XidynException(final Throwable cause)
    {
        super(cause);
    }
}
